#include "TransformStack.h"
#include "Cylinder.h"

static void Tree(TransformStack& t, Cylinder& model, int shaderProg, int iteration, double time, int& index)
{
	if (iteration < 16)
	{
		index++;
		iteration++;
		float ampl = pow(sin(iteration - time*355.0/133.0*.5)*.5 + .5, 8.0)*1.4;
		//ampl = pow(sin(index + time*.1)*.5 + .5, 640.0)*2.0;
		float dist = time*.01 + .2;

		t.push();
		t.apply(Matrix4f::translation(sin(time*.1)*2.0*dist, sin(time*.071)*2.0*dist, sin(time*.041)*2.0*dist));
		t.apply(Matrix4f::rotateX(.4 + time*.04, false));
		t.apply(Matrix4f::rotateY(.4 + time*.04, false));
		t.apply(Matrix4f::rotateZ(.4 + time*.04, false));
		t.apply(Matrix4f::scale(.7, .7, .7));
		model.setAmbient(.1*ampl + .1, .4*ampl + .1, .9*ampl + .1);
		model.setTransform(t);
		model.draw(shaderProg);
		Tree(t, model, shaderProg, iteration, time, index);
		t.pop();
	}
}

void RenderEffect0(int shaderProg, double time)
{
	time += int(time*.125)*2.0;
	Cylinder model(8);
	model.setAmbient(.1, .1, .1);
	model.setDiffuse(.1, .1, .1);
	model.setSpecular(.1, .1, .1);
	int index = 0;
	TransformStack t;
	t.apply(Matrix4f::translation(-1.0,.0,.0));
	for (int i = 0; i < 16; i++)
	{
		Tree(t, model, shaderProg, 0, time*355.0 / 113.0, index);
		t.apply(Matrix4f::rotateX(86.0, true));
		t.apply(Matrix4f::rotateY(66.0, true));
		t.apply(Matrix4f::rotateZ(76.0, true));
	}

}

static void Tree1(TransformStack& t, Cylinder& model, int shaderProg, int iteration, double time_, int& index)
{
	if (iteration < 16)
	{
		float time = time_* .9;
		index++;
		int current_index = index;
		iteration++;
		float ampl2 = pow(sin(iteration + time*.5)*.5 + .5, 16.0)*2.0;
		float ampl = pow(sin(index + time*.50)*.5 + .5, 640.0)*2.0;
		float dist = iteration*iteration*iteration*.001;

		t.push();
		t.apply(Matrix4f::translation(sin(time*.1)*2.0*dist, sin(time*.071)*2.0*dist, sin(time*.041)*2.0*dist));
		t.apply(Matrix4f::rotateX(time*.02, false));
		t.apply(Matrix4f::rotateY(time*.01, false));
		t.apply(Matrix4f::rotateZ(time*.005, false));
		//t.apply(Matrix4f::rotateZ(.2 + sin(time*.05), false));
		t.apply(Matrix4f::scale(.95, .95, .95));
		model.setAmbient(.1*ampl + .9*ampl2 + .05, .4*ampl + .4*ampl2 + .05, .9*ampl + .3*ampl2 + .05);
		model.setTransform(t);
		if (iteration>10)
			model.draw(shaderProg);
		Tree1(t, model, shaderProg, iteration, time, index);

		if (current_index % 7 == 0)
		{
			t.apply(Matrix4f::rotateX(4.1, false));
			t.apply(Matrix4f::rotateY(3.2, false));
			t.apply(Matrix4f::rotateZ(2.3, false));
			Tree1(t, model, shaderProg, iteration, time, index);
		}

		if (current_index % 9 == 0)
		{
			t.apply(Matrix4f::rotateX(2.1, false));
			t.apply(Matrix4f::rotateY(3.2, false));
			t.apply(Matrix4f::rotateZ(1.3, false));
			Tree1(t, model, shaderProg, iteration, time, index);

		}

		if (current_index % 5 == 0)
		{
			t.apply(Matrix4f::rotateX(1.2, false));
			t.apply(Matrix4f::rotateY(2.3, false));
			t.apply(Matrix4f::rotateZ(3.1, false));
			Tree1(t, model, shaderProg, iteration, time, index);

		}

		if (current_index % 8 == 0)
		{
			t.apply(Matrix4f::rotateX(.3, false));
			t.apply(Matrix4f::rotateY(.2, false));
			t.apply(Matrix4f::rotateZ(.1, false));
			Tree1(t, model, shaderProg, iteration, time, index);
		}

		t.pop();
	}
}

void RenderEffect1(int shaderProg, double time)
{
	Cylinder model(8);
	model.setAmbient(.1, .1, .1);
	model.setDiffuse(.1, .1, .1);
	model.setSpecular(.1, .1, .1);
	int index = 0;
	TransformStack t;
	for (int i = 0; i < 1; i++)
	{
		Tree1(t, model, shaderProg, 0, time*355.0 / 113.0, index);
		t.apply(Matrix4f::rotateX(86.0, true));
		t.apply(Matrix4f::rotateY(66.0, true));
		t.apply(Matrix4f::rotateZ(76.0, true));
	}

}


void RenderParticles(int shaderProg, double time)
{
	Cylinder model(4);
	model.setAmbient(.3, .3, .3);
	model.setDiffuse(.1, .1, .1);
	model.setSpecular(.1, .1, .1);
	TransformStack t;
	double speed = .5;
	for (int i = 0; i < 300; i++)
	{
		t.apply(Matrix4f::rotateX(86.0, true));
		t.apply(Matrix4f::rotateY(66.0, true));
		t.apply(Matrix4f::rotateZ(76.0, true));
		t.push();
		t.apply(Matrix4f::translation(
			sin(time*.100*speed + i*232.12)*20.0,
			sin(time*.071*speed + i*532.12)*20.0,
			sin(time*.041*speed + i*332.12)*20.0
			));
		t.apply(Matrix4f::scale(.1, .1, .1));
		model.setTransform(t);
		model.draw(shaderProg);
		t.pop();
	}
}

void RenderArc(int shaderProg, double time)
{
	Cylinder model(8);
	model.setAmbient(.1, .1, .1);
	model.setDiffuse(.051, .051, .051);
	model.setSpecular(.1, .1, .1);
	TransformStack t;
	int limit = 10;
	int count = time*4.0;
	if (count > limit) count = limit;
	t.apply(Matrix4f::rotateZ(sin(1.0 + time*.1)*.1, false));
	t.apply(Matrix4f::rotateY(-3.0/(1.0+time), false));
	for (int i = 0; i < count; i++)
	{
		float ampl = pow(cos(i*.5 -time*355.0/113.0*2.0)*.5 + .5, 16.0)+.2;
		t.push();
		t.apply(Matrix4f::rotateZ(10.0*(i-5.0), true));
		t.apply(Matrix4f::translation(3.0+sin(time*.3)*.1, .0, .0));
		t.apply(Matrix4f::rotateZ(90, true));
		t.apply(Matrix4f::scale(.125, .5, 0.125));
		model.setAmbient(.1*ampl, .9*ampl, .4*ampl);
		model.setTransform(t);
		t.pop();
		model.draw(shaderProg);
	}
}

void RenderSemmi(int shaderProg, double time)
{
	Cylinder model(8);
	model.setAmbient(.1, .1, .1);
	model.setDiffuse(.051, .051, .051);
	model.setSpecular(.1, .1, .1);
	TransformStack t;
	for (int x = -10; x <= 10; x++){
		for (int z = -10; z <= 10; z++){
			t.push();
			t.apply(Matrix4f::translation(x + fmod(time, 1.0), .0, z));
			model.setTransform(t);
			model.draw(shaderProg);
			t.pop();
		}
	}
}

void RenderCylinderField(int shaderProg, double time)
{
	Cylinder model(16);
	model.setAmbient(.1, .1, .1);
	model.setDiffuse(.051, .051, .051);
	model.setSpecular(.1, .1, .1);
	TransformStack t;
	t.apply(Matrix4f::translation(.0, -2.0, -10.0));
	int section = int(time*.125) & 7;
	float rotatecoef[] = { .15, .15, .15, -.15, .1, -.1, .15, -.15 };
	float rotatecoef2[] = { .0, .0, .0, -.2, .2, -.2, .3, -.2, };
	t.apply(Matrix4f::rotateX(1.0 + rotatecoef2[section], false));
	t.apply(Matrix4f::rotateY(1.0 + time*rotatecoef[section], false));
	for (int x = -15; x <= 15; x++){
		for (int z = -15; z <= 15; z++){
			t.push();
			float len = sqrt(x*x + z*z);
			float y = sin(len*1.5 - time)* (1.0 - 1.0 / (1.0 + time*.1));
			float ampl = .0; float a = time;
			switch (section){
			case 0: ampl = pow(sin(len - time)*.5 + .5, 16.0); break;
			case 1: ampl = pow(sin(x*.25 - time*2.0)*.5 + .5, 16.0); break;
			case 2: ampl = pow(sin(z*.25 - time*2.0)*.5 + .5, 16.0); break;
			case 3: ampl = pow(sin(len - time)*.5 + .5, 16.0); break;
			case 4: ampl = pow(sin(z*cos(a)*.5 + x*sin(a)*.5 - time*2.0)*.5 + .5, 16.0); break;
			case 5: ampl = pow(sin(len*.5 - time*4.0)*.5 + .5, 32.0); break;
			case 6: ampl = pow(sin(y*2.5)*.5 + .5, 32.0); break;
			case 7: ampl = pow(sin(sin(x*.2 + time + sin(y*.2 - time))*sin(y*.2 + time + sin(x*.2 - time))*4.0)*.5 + .5, 32.0); break;
			}
			ampl *= (1.0 - 1.0 / (1.0 + (time - 8.0)*.1));;
			if (ampl < .0) ampl = .0;
			model.setAmbient(.1 + .9*ampl, .1 + .5*ampl, .1 + .3*ampl);
			t.apply(Matrix4f::translation(x, y, z));
			t.apply(Matrix4f::scale(.5, 2.5, .5));
			model.setTransform(t);
			model.draw(shaderProg);
			t.pop();
		}
	}
}


void RenderCity(int shaderProg, double time)
{
	Cylinder model(16);
	model.setAmbient(.1, .1, .1);
	model.setDiffuse(.051, .051, .051);
	model.setSpecular(.1, .1, .1);
	TransformStack t;
	t.apply(Matrix4f::translation(.0, -2.0, -13.0));
	int section = int(time*.125) & 7;
	float rotatecoef[] = { .03, -.05, .15, -.15, .1, -.1, .15, -.15 };
	float rotatecoef2[] = { .0, .0, .0, -.1, .1, -.1, .1, -.1, };
	t.apply(Matrix4f::rotateX(.1 + rotatecoef2[section], false));
	t.apply(Matrix4f::rotateY(1.0 + time*rotatecoef[section], false));
	for (int x = -15; x <= 15; x++){
		for (int z = -15; z <= 15; z++){
			float len = sqrt(x*x + z*z);
			float y = pow(sin(x*x + z*z + 5.12 +x*97.2 +z*52.1)*.5+.5,10.0) / (len*.5 + 0.2)*8.0;
			float ampl = pow(sin(len*.25 - time*355.0 / 113.0*.5)*.5 + .5, 16.0);
			ampl *= (1.0 - 1.0 / (1.0 + (time - 8.0)*.1));;
			if (ampl < .0) ampl = .0;
			model.setAmbient(.1 + .3*ampl, .1 + 1.5*ampl, .1 + 1.3*ampl);
			if (y > .25)
			{
				t.push();
				t.apply(Matrix4f::translation(x, y, z));
				t.apply(Matrix4f::scale(.5, y, .5));
				model.setTransform(t);
				model.draw(shaderProg);
				t.pop();
			}
		}
	}

	t.apply(Matrix4f::translation(0, 0, 0));
	t.apply(Matrix4f::scale(20.0, 0.1, 20.0));
	model.setTransform(t);
	model.draw(shaderProg);
}