#include "Vector.h"

class Light {
public:
	Vector4f position;
	Vector4f ambient;
	Vector4f diffuse;
	Vector4f specular;
	Vector4f lookAtPoint;
	float angularAtten;
	float coneAngle;

	Light() {
		this->position = Vector4f(0, 0, 0, 0);
		this->ambient = Vector4f(0, 0, 0, 0);
		this->diffuse = Vector4f(0, 0, 0, 0);
		this->specular = Vector4f(0, 0, 0, 0);

		// Used only for spotlights
		this->lookAtPoint = Vector4f(0, 0, 0, 0);
		this->angularAtten = 23;
		this->coneAngle = 35;
	}

	void setAmbient(float r, float g, float b) {
		this->ambient = Vector4f(r, g, b, 0.0);
	}

	void setDiffuse(float r, float g, float b) {
		this->diffuse = Vector4f(r, g, b, 0.0);
	}

	void setSpecular(float r, float g, float b) {
		this->specular = Vector4f(r, g, b, 0.0);
	}

	void setPosition(float x, float y, float z) {
		this->position = Vector4f(x, y, z, 0);
	}

	// Below are properties only used for spotlights
	void setLookAtPoint(float x, float y, float z) {
		this->lookAtPoint = Vector4f(x, y, z, 0);
	}

	void setAngularAttenuation(float att) {
		this->angularAtten = att;
	}

	void setConeAngle(float angle) {
		this->coneAngle = angle;
	}
};
