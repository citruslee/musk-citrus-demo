#include "Video.h"
#include <stdio.h>

extern "C" 
{ 
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001; 
}

Video::Video(){}
Video::~Video(){}

bool Video::Create(LPCWSTR title, bool fullscreen, unsigned int width, unsigned int height)
{
	m_title = title;
	m_fullscreen = fullscreen;
	m_width = width;
	m_height = height;

	WNDCLASS windowClass;
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = Video::WndProc;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = NULL; //Don't need hInstance
	windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground = NULL;
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = title;
	if (!RegisterClass(&windowClass))
	{
		int error = (int)GetLastError();
		char strbuf[256];
		sprintf(strbuf, "Couldn't Register Window Class, ErrorCode: %i", error);
		MessageBoxA(NULL, strbuf, "ERROR", MB_OK);
		printf("Failed to register window class\n");
		return false;
	}

	DEVMODE devScrSettings;
	memset(&devScrSettings, 0, sizeof(devScrSettings));
	devScrSettings.dmSize = sizeof(devScrSettings);
	devScrSettings.dmPelsWidth = width;
	devScrSettings.dmPelsHeight = height;
	devScrSettings.dmBitsPerPel = 32;
	devScrSettings.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;
	DWORD dwExStyle;
	DWORD dwStyle;
	if (m_fullscreen)
	{
		dwExStyle = WS_EX_APPWINDOW;
		dwStyle = WS_POPUP;

		ChangeDisplaySettings(&devScrSettings, CDS_FULLSCREEN);
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		dwStyle = WS_OVERLAPPEDWINDOW;
	}
	//Look Ma, no hInstance
	m_hWnd = CreateWindowEx(dwExStyle, title, title, WS_CLIPSIBLINGS | WS_CLIPCHILDREN | dwStyle, CW_USEDEFAULT, 0, width, height, NULL, NULL, NULL, this);

	if (!m_hWnd)
	{
		int error = (int)GetLastError();
		char strbuf[256];
		sprintf(strbuf, "Failed to create base window before Video Acceleration\nErrorCode: %i", error);
		MessageBoxA(NULL, strbuf, "ERROR", MB_OK);
		return false;
	}

	m_hDC = GetDC(m_hWnd); // Get the device context for our window

	PIXELFORMATDESCRIPTOR pfd; // Create a new PIXELFORMATDESCRIPTOR (PFD)
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR)); // Clear our  PFD
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR); // Set the size of the PFD to the size of the class
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW; // Enable double buffering, opengl support and drawing to a window
	pfd.iPixelType = PFD_TYPE_RGBA; // Set our application to use RGBA pixels
	pfd.cColorBits = 32; // Give us 32 bits of color information (the higher, the more colors)
	pfd.cDepthBits = 32; // Give us 32 bits of depth information (the higher, the more depth levels)
	pfd.iLayerType = PFD_MAIN_PLANE; // Set the layer of the PFD

	int nPixelFormat = ChoosePixelFormat(m_hDC, &pfd); // Check if our PFD is valid and get a pixel format back
	if (nPixelFormat == 0) // If it fails
	{
		MessageBoxA(NULL, "Failed to choose pixel format", "ERROR", MB_OK);
		return false;
	}

	bool bResult = SetPixelFormat(m_hDC, nPixelFormat, &pfd); // Try and set the pixel format based on our PFD
	if (!bResult) // If it fails
	{
		MessageBoxA(NULL, "Failed to set pixel format", "ERROR", MB_OK);
		return false;
	}

	HGLRC tempOpenGLContext = wglCreateContext(m_hDC); // Create an OpenGL 2.1 context for our device context
	wglMakeCurrent(m_hDC, tempOpenGLContext); // Make the OpenGL 2.1 context current and active

	int attributes[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3, // Set the MAJOR version of OpenGL to 3
		WGL_CONTEXT_MINOR_VERSION_ARB, 3, // Set the MINOR version of OpenGL to 3
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, // Set our OpenGL context to be forward compatible
		0
	};

	// Initialize GLEW
	int glewExperimental = TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK)
	{
		return false;
	}

	PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
	if (wglCreateContextAttribsARB){
		m_hGLRC = wglCreateContextAttribsARB(m_hDC, NULL, attributes); // Create and OpenGL 3.x context based on the given attributes
		wglMakeCurrent(NULL, NULL); // Remove the temporary context from being active
		wglDeleteContext(tempOpenGLContext); // Delete the temporary OpenGL 2.1 context
		wglMakeCurrent(m_hDC, m_hGLRC); // Make our OpenGL 3.2 context current

		PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
		wglSwapIntervalEXT(0); //VSYNC NOT LOCKED TO 60
	}
	else {
		m_hGLRC = tempOpenGLContext; // If we didn't have support for OpenGL 3.x and up, use the OpenGL 2.1 context
		printf("No OpenGL 3.3 Support, fallback Version 2.1");
	}

	//LoadGLExtensions(); //Load all the extensions!

	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);

	glClearColor(0.4f, 0.6f, 0.9f, 0.0f);
	glFrontFace(GL_CCW);
	glDisable(GL_CULL_FACE);
	//glCullFace(GL_FRONT_AND_BACK);
	glEnable(GL_DEPTH_TEST);

	return true;
}

void Video::RenderBegin(float bg_r, float bg_g, float bg_b)
{
	setTitleFPS();
	glClearColor(bg_r, bg_g, bg_b, 1.0f);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Video::RenderEnd()
{
	//OPENGL:
	SwapBuffers(m_hDC);
}

void Video::setTitleFPS()
{
	static unsigned int fps = 0;
	static DWORD fpsLast = GetTickCount();
	static char windowText[255];
	static DWORD curTick = 0;

	++fps;
	curTick = GetTickCount();

	if (curTick - fpsLast > 1000) //Once per Second
	{
		sprintf(windowText, "%ls %ls %i", m_title, L"FPS:", fps);
		SetWindowTextA(m_hWnd, windowText);
		fps = 0;
		fpsLast = curTick;
	}
}

void Video::resizeWindow(unsigned int width, unsigned int height)
{
	m_width = width;
	m_height = height;

	//OPENGL:
	glViewport(0, 0, width, height);
}

/*LRESULT CALLBACK Video::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Video* video = 0;
	switch (message) {
	case WM_CREATE:
	{
		video = (Video*)((LPCREATESTRUCT)lParam)->lpCreateParams;
		SetWindowLongPtr(hWnd, GWL_USERDATA, (LONG_PTR)video);
	}
	case WM_SIZE: // If our window is resizing
	{
		video = (Video*)GetWindowLongPtr(hWnd, GWL_USERDATA);
		if (!video)
			MessageBoxA(NULL, "Bad Window Pointer on Resize", "Error", MB_OK);
		video->resizeWindow(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		break;
	}
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_UP:
			
		}
		break;
	}
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}*/
