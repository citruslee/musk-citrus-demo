#pragma once  

// Include Windows functions  
#include <Windows.h>

// Include input and output operations  
#include <iostream>

// OpenGL and GLEW Header Files and Libraries  
#include <gl\GL.h>
#pragma comment(lib, "opengl32.lib")