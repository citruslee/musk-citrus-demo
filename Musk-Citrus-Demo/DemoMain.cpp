#include "Video.h"
#include "Vector.h"
#include "Light.h"
#include "Camera.h"
#include "Shader.h"
#include "Sphere.h"
#include "Cylinder.h"
#include "Cube.h"
#include "FullscreenQuad.h"
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <Windows.h>
#include <shellapi.h>
#include <string.h>
#include <stdlib.h>
#include <wchar.h>


#include "../audio/Music.h"

#include "Framebuffer.h"

#include "Effects.h"
#include "Text.h"

#define KEYBOARD_CONTROL

const GLfloat PITCH_AMT = 1.0; // degrees up and down
const GLfloat YAW_AMT = 5.0; // degrees right and left
const GLfloat FORWARD_AMT = 0.5;
const GLfloat TIMER_TICK = 20; // milliseconds

Vector3f position(0.0, 0.0, 10.0);
Vector3f lookAtPoint(0, 0, 0);
Vector3f upVector(0, 1, 0);
Camera *cam;
Light *light;
Light *spotlight;

GLfloat shininess = 5.0; // min is zero
GLfloat SHINY_FACTOR = 5.0;

GLuint shaderProg;
GLuint postproc;
GLuint chromaticaberration;
GLuint textshader;

//post shaders
GLuint post_intro;
GLuint post_effect0;
GLuint post_effect1;
GLuint post_city;
GLuint post_arc;
GLuint post_cylinderfield;
GLuint post_end;

GLfloat angularAtten = 55;
GLfloat coneAngle = 40;

int APIENTRY WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	LPWSTR *szArgList;
	int argCount;

	szArgList = CommandLineToArgvW(GetCommandLine(), &argCount);
	if (szArgList == NULL)
	{
		MessageBox(NULL, L"Unable to parse command line", L"Error", MB_OK);
		return 10;
	}

	for (int i = 0; i < argCount; i++)
	{
		//MessageBox(NULL, szArgList[i], L"Arglist contents", MB_OK);
		if (lstrcmpW(szArgList[i], L"--res") == 0)
		{
			SCR_W = _wtoi(szArgList[i + 1]);
			SCR_H = _wtoi(szArgList[i + 2]);
		}
	}

	LocalFree(szArgList);
#ifdef _DEBUG
	AllocConsole();

	HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
	int hCrt = _open_osfhandle((long)handle_out, _O_TEXT);
	FILE* hf_out = _fdopen(hCrt, "w");
	setvbuf(hf_out, NULL, _IONBF, 1);
	*stdout = *hf_out;

	HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
	hCrt = _open_osfhandle((long)handle_in, _O_TEXT);
	FILE* hf_in = _fdopen(hCrt, "r");
	setvbuf(hf_in, NULL, _IONBF, 128);
	*stdin = *hf_in;

	HANDLE handle_err = GetStdHandle(STD_ERROR_HANDLE);
	hCrt = _open_osfhandle((long)handle_err, _O_TEXT);
	FILE* hf_err = _fdopen(hCrt, "w");
	setvbuf(hf_err, NULL, _IONBF, 128);
	*stderr = *hf_err;
	Video video;
	if (!video.Create(L"Demo", false, SCR_W, SCR_H))
	{
		MessageBoxA(NULL, "Failed to create window!", "Error", MB_OK);
		return 1;
	}
#else
	Video video;
	if (!video.Create(L"Demo", true, SCR_W, SCR_H))
	{
		MessageBoxA(NULL, "Failed to create window!", "Error", MB_OK);
		return 1;
	}
	ShowCursor(FALSE);
#endif
	

	Music::Init("mzk.mp3", true);

	
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	
	Shader s;
	s.createShaderProgram("phong.vert", "phong.frag", &shaderProg);

	//Shader s;
	s.createShaderProgram("screenquad.vert", "screenquad.frag", &postproc);
	s.createShaderProgram("screenquad.vert", "chromaticaberration.frag", &chromaticaberration);
	s.createShaderProgram("screenquad.vert", "textshader.frag", &textshader);

	//load post shaders

	s.createShaderProgram("screenquad.vert", "post_intro.frag", &post_intro);
	s.createShaderProgram("screenquad.vert", "post_effect0.frag", &post_effect0);
	s.createShaderProgram("screenquad.vert", "post_city.frag", &post_city);
	s.createShaderProgram("screenquad.vert", "post_effect1.frag", &post_effect1);
	s.createShaderProgram("screenquad.vert", "post_arc.frag", &post_arc);
	s.createShaderProgram("screenquad.vert", "post_cylinderfield.frag", &post_cylinderfield);
	s.createShaderProgram("screenquad.vert", "post_end.frag", &post_end);
	
	FBO rt;
	FBO postpro;

	Text *greetingsto = new Text("Texts/txt-L1Normal255HGreetings To.png");
	Text *darklite = new Text("Texts/txt-L2Normal255HDarklite.png");
	Text *conspiracy = new Text("Texts/txt-L3Normal255HConspiracy.png");
	Text *pandacube = new Text("Texts/txt-L4Normal255HPandaCube.png");
	Text *unitedforce = new Text("Texts/txt-L5Normal255HUnited Force.png");
	Text *farbrausch = new Text("Texts/txt-L6Normal255HFarbrausch.png");
	Text *creators = new Text("Texts/txt-L7Normal255HCreators.png");
	Text *demoname = new Text("Texts/txt-L8Normal255HDemo Name.png");
	Text *partiarc = new Text("Texts/txt-L9Normal255HPartiarc.png");
	Text *specialthanks = new Text("Texts/txt-L10Normal255VSpecial Thanks.png");

	rt.Create();
	postpro.Create();

	Music::Play();

	cam = new Camera(position, lookAtPoint, upVector);
	cam->fovX();
	// Set up light
	light = new Light();

	// white light
	light->setAmbient(1.0, 1.0, 1.0);
	light->setDiffuse(1.0, 1.0, 1.0);
	light->setSpecular(1.0, 1.0, 1.0);
	light->setPosition(10, 10, 10);

	spotlight = new Light();
	spotlight->setAmbient(1.0, 1.0, 1.0);
	spotlight->setDiffuse(1.0, 1.0, 1.0);
	spotlight->setSpecular(1.0, 1.0, 1.0);
	spotlight->setPosition(10, 10, 10);
	spotlight->setAngularAttenuation(angularAtten);
	spotlight->setConeAngle(coneAngle);
	spotlight->setLookAtPoint(0, 0, 0);

	SolidSphere *sph = new SolidSphere(1, 50, 50);
	SolidCube *cube = new SolidCube();
	Cylinder *cyl = new Cylinder(20);

	FullscreenQuad *fq = new FullscreenQuad();

	
	MSG msg = {};
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			double time = Music::GetPositionByte() / 44100.0 / 2.0 / 2.0 / 60.0*150.0;

			video.RenderBegin(.09,.05,.06);
			rt.Bind();
			
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);

			glUseProgram(shaderProg);

			GLuint viewMatLoc = glGetUniformLocation(shaderProg, "viewMat");
			glUniformMatrix4fv(viewMatLoc, 1, 1, (float *)cam->getViewMatrix().vm);

			GLuint projMatLoc = glGetUniformLocation(shaderProg, "projMat");
			glUniformMatrix4fv(projMatLoc, 1, 1, (float *)cam->getProjMatrix().vm);

			GLuint lightAmbLoc = glGetUniformLocation(shaderProg, "lightAmb");
			glUniform4fv(lightAmbLoc, 1, (float *)&light->ambient);

			GLuint lightDiffLoc = glGetUniformLocation(shaderProg, "lightDiff");
			glUniform4fv(lightDiffLoc, 1, (float *)&light->diffuse);

			GLuint lightSpecLoc = glGetUniformLocation(shaderProg, "lightSpec");
			glUniform4fv(lightSpecLoc, 1, (float *)&light->specular);

			GLuint lightPosLoc = glGetUniformLocation(shaderProg, "lightPos");
			glUniform4fv(lightPosLoc, 1, (float *)&light->position);

			GLuint shininessLoc = glGetUniformLocation(shaderProg, "shininess");
			glUniform1f(shininessLoc, shininess);

			GLuint spotPosLoc = glGetUniformLocation(shaderProg, "spotPos");
			glUniform4fv(spotPosLoc, 1, (float *)&spotlight->position);

			GLuint spotLookAtLoc = glGetUniformLocation(shaderProg, "spotLookAtPnt");
			glUniform4fv(spotLookAtLoc, 1, (float *)&spotlight->lookAtPoint);

			GLuint spotAngAttenLoc = glGetUniformLocation(shaderProg, "spotAngAtten");
			glUniform1f(spotAngAttenLoc, spotlight->angularAtten);

			GLuint spotConeAngleLoc = glGetUniformLocation(shaderProg, "spotConeAngle");
			glUniform1f(spotConeAngleLoc, spotlight->coneAngle);

			GLuint camPosLoc = glGetUniformLocation(shaderProg, "camPos");
			glUniform4fv(camPosLoc, 1, (float *)&cam->position);

#define POSTENDRENDER(ID) {\
			rt.Unbind();\
			postpro.Bind();\
			glUseProgram(postproc);\
			GLuint texID = glGetUniformLocation(postproc, "renderedTexture");\
			glActiveTexture(GL_TEXTURE0);\
			glBindTexture(GL_TEXTURE_2D, rt.renderedTexture);\
			glUniform1i(texID, 0);\
			fq->draw(postproc);\
			postpro.Unbind();\
			glUseProgram(ID);\
			texID = glGetUniformLocation(ID, "renderedTexture");\
			glActiveTexture(GL_TEXTURE0);\
			glBindTexture(GL_TEXTURE_2D, postpro.renderedTexture);\
			int resloc = glGetUniformLocation(ID, "resolution");\
			glUniform2f(resloc, SCR_W, SCR_H);\
			int fragment_time = glGetUniformLocation(ID, "time");\
			glUniform1f(fragment_time, local_time);\
			glUniform1i(texID, 0);\
			fq->draw(chromaticaberration);\
			glUseProgram(0);\
			video.RenderEnd();}\

			//RENDER CODE
			double time_per_section = 32.0;
			int section = int(time / time_per_section);
			double time_per_minisection = 4.0;
			int minisection = int(time / time_per_minisection);
			double local_time = time - section*time_per_section;
			switch (section)
			{
			case 0:
				RenderSemmi(shaderProg, time);
				glUseProgram(textshader);
				demoname->draw(textshader);
				POSTENDRENDER(post_intro);
				break;
			case 1:
				RenderEffect0(shaderProg, time - section*time_per_section);
				RenderParticles(shaderProg, time);
				POSTENDRENDER(post_effect0);
				break;
			case 2:
				RenderCity(shaderProg, time - section*time_per_section);
				RenderParticles(shaderProg, time);
				POSTENDRENDER(post_city);
				break;
			case 3:
				RenderCity(shaderProg, time - (section - 1)*time_per_section);
				RenderParticles(shaderProg, time);
				POSTENDRENDER(post_city);
				break;
			case 4:
				RenderEffect1(shaderProg, time - section*time_per_section);
				RenderParticles(shaderProg, time);
				POSTENDRENDER(post_effect1);
				break;
			case 5:
				RenderArc(shaderProg, time - (section*time_per_section));
				RenderParticles(shaderProg, time);
				glUseProgram(textshader);
				partiarc->draw(textshader);
				POSTENDRENDER(post_arc);
				break;
			case 6:
				RenderCylinderField(shaderProg, time - section*time_per_section);
				RenderParticles(shaderProg, time);
				POSTENDRENDER(post_cylinderfield);
				break;
			case 7:
				RenderCylinderField(shaderProg, time - (section-1)*time_per_section);
				RenderParticles(shaderProg, time);
				POSTENDRENDER(post_cylinderfield);
				break;
			default:
				RenderCylinderField(shaderProg, time - (section - 1)*time_per_section);
				RenderParticles(shaderProg, time);
				switch (minisection - 64)
				{
				case 0:
					glUseProgram(textshader);
					greetingsto->draw(textshader);
					POSTENDRENDER(chromaticaberration);
					break;
				case 1:
					glUseProgram(textshader);
					darklite->draw(textshader);
					POSTENDRENDER(post_end);
					break;
				case 2:
					glUseProgram(textshader);
					conspiracy->draw(textshader);
					POSTENDRENDER(post_end);
					break;
				case 3:
					glUseProgram(textshader);
					farbrausch->draw(textshader);
					POSTENDRENDER(post_end);
					break;
				case 4:
					glUseProgram(textshader);
					pandacube->draw(textshader);
					POSTENDRENDER(post_end);
					break;
				case 5:
					glUseProgram(textshader);
					unitedforce->draw(textshader);
					POSTENDRENDER(post_end);
					break;
				case 6:
					glUseProgram(textshader);
					creators->draw(textshader);
					POSTENDRENDER(chromaticaberration);
					break;
				case 7:
				case 8:
					glUseProgram(textshader);
					specialthanks->draw(textshader);
					POSTENDRENDER(chromaticaberration);
					break;
				case 9:
				case 10:
				case 11:
					glUseProgram(textshader);
					demoname->draw(textshader);
					POSTENDRENDER(chromaticaberration);
					break;
				default:
					PostQuitMessage(0);
				}
				break;
			}

			/*
			rt.Unbind();

			postpro.Bind();

			// Use our shader
			glUseProgram(postproc);
			GLuint texID = glGetUniformLocation(postproc, "renderedTexture");
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, rt.renderedTexture);

			glUniform1i(texID, 0);

			fq->draw(postproc);

			postpro.Unbind();

			glUseProgram(chromaticaberration);
			texID = glGetUniformLocation(chromaticaberration, "renderedTexture");

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, postpro.renderedTexture);

			int resloc = glGetUniformLocation(chromaticaberration, "resolution");
			glUniform2f(resloc, SCR_W, SCR_H);

			glUniform1i(texID, 0);

			fq->draw(chromaticaberration);

			glUseProgram(0);
			
			video.RenderEnd();*/
		}
	}
	return (int)msg.wParam;
}

LRESULT CALLBACK Video::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Video* video = 0;
	switch (message) {
	case WM_CREATE:
	{
		video = (Video*)((LPCREATESTRUCT)lParam)->lpCreateParams;
		SetWindowLongPtr(hWnd, GWL_USERDATA, (LONG_PTR)video);
	}
	case WM_SIZE: // If our window is resizing
	{
		video = (Video*)GetWindowLongPtr(hWnd, GWL_USERDATA);
		if (!video)
			MessageBoxA(NULL, "Bad Window Pointer on Resize", "Error", MB_OK);
		video->resizeWindow(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		break;
	}
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
#ifdef KEYBOARD_CONTROL
		case VK_UP:
			//cam->moveForward(FORWARD_AMT);
			Music::SetPositionByte(Music::GetPositionByte()+441000);
			break;
		case VK_DOWN:
			//cam->moveForward(-FORWARD_AMT);
			Music::SetPositionByte(Music::GetPositionByte() - 441000);
			break;
		case VK_LEFT:
			//cam->yaw(-YAW_AMT);
			Music::SetPositionByte(Music::GetPositionByte() + 44100);
			break;
		case VK_RIGHT:
			//cam->yaw(YAW_AMT);
			Music::SetPositionByte(Music::GetPositionByte() - 44100);
			break;
#endif
		case VK_ESCAPE:
			PostQuitMessage(0);
			break;
		}
		break; 
	}
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}
