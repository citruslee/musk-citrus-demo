#include "stdlib.h"
#include "stdio.h"
#include "glew.h"
#include "Camera.h"

/**
* Camera constructor
*/
Camera::Camera(Vector3f posVec, Vector3f lookAtPoint, Vector3f upVec) {
	this->position = posVec;
	this->lookAtVector = lookAtPoint - position;
	this->upVector = upVec;

	this->modelMat = Matrix4f::identity();
	// setting up the viewpoint transformation
	this->viewMat = Matrix4f::cameraMatrix(this->position, this->lookAtVector, this->upVector);
	// setting up the projection transformation
	this->projMat = Matrix4f::symmetricPerspectiveProjectionMatrix(35, width / height, 1.0, 1000);
	this->refresh();
}

void Camera::reshape(GLfloat w, GLfloat h) {
	this->width = w;
	this->height = h;
	this->projMat = Matrix4f::symmetricPerspectiveProjectionMatrix(60, w / h, 1.0, 1000);
	this->refresh();
}

Camera::~Camera(void) {
	// destruct the camera
}

int Camera::roll(float angleDeg) {
	GLfloat angle = DegreeToRadians(angleDeg);

	// calculate the angle between upVector and rightVector to get roll angle
	this->upVector = (upVector * cos(angle) + rightVector * sin(angle)).normalize();
	this->rightVector = Vector3f::cross(lookAtVector, upVector);
	// this->lookAtVector = Vector3f::cross(rightVector, upVector);
	this->refresh();

	return 0;
}

int Camera::pitch(float angleDeg) {
	GLfloat angle = DegreeToRadians(angleDeg);

	// calculate the angle between lookAtVector and upVector to get pitch angle
	this->lookAtVector = (lookAtVector * cos(angle) + upVector * sin(angle)).normalize();
	this->upVector = Vector3f::cross(rightVector, lookAtVector);
	// this->rightVector = Vector3f::cross(upVector, lookAtVector);
	this->refresh();

	return 0;
}

int Camera::yaw(float angleDeg) {
	GLfloat angle = DegreeToRadians(angleDeg);
	// calculate angle between lookAtVector and rightVector to get yaw angle
	this->lookAtVector = (lookAtVector * cos(angle) + rightVector * sin(angle)).normalize();
	this->rightVector = Vector3f::cross(lookAtVector, upVector);
	// need to update up vector because rightVector is always interpreted from others.
	this->upVector = Vector3f::cross(rightVector, lookAtVector);

	this->refresh();

	return 0;
}

Vector3f Camera::getPosition(void) {
	return position;
}

Vector3f Camera::getLookAtPoint(void) {
	return (position + lookAtVector);
}

Vector3f Camera::getUpVector(void) {
	return upVector;
}

int Camera::changeAbsPosition(float x, float y, float z)
{
	position.x = x;
	position.y = y;
	position.z = z;
	return 0;
}

int Camera::changePositionDelta(float dx, float dy, float dz) {
	position.x += dx;
	position.y += dy;
	position.z += dz;

	return 0;
}

Vector3f Camera::moveForward(float numUnits) {
	this->position = position + (lookAtVector * numUnits);
	this->refresh();
	return position;
}

Vector3f Camera::moveRight(float numUnits) {
	this->position = position + (rightVector * numUnits);
	this->refresh();
	return position;
}

Matrix4f Camera::getViewMatrix() {
	return viewMat;
}

GLfloat Camera::fovX() {
	return 60 * (width / height);
}

Matrix4f Camera::getProjMatrix() {
	return projMat;
}

void Camera::setCamera(Vector3f position, Vector3f lookAtPoint, Vector3f upVector) {
	this->position = position;
	this->lookAtVector = lookAtPoint - position;
	this->upVector = upVector;
	this->rightVector = Vector3f::cross(lookAtVector, upVector);
	this->upVector.normalize();
	this->lookAtVector.normalize();
	this->rightVector.normalize();

	this->viewMat = Matrix4f::cameraMatrix(this->position, this->getLookAtPoint(), this->upVector);
}

void Camera::refresh(void) {
	this->setCamera(this->position, this->getLookAtPoint(), this->upVector);
}

int Camera::updateSpeed(float speed) {
	this->speed += speed;
	return 0;
}

float Camera::getSpeed(void) {
	return(speed);
}