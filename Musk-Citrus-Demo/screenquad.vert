#version 330

in vec4 vPosition;
in vec2 vUv;

out vec2 uv;
void main() 
{
	uv = vUv;
    gl_Position = vec4(vPosition.xy, 0.0, 1.0 );
   
}