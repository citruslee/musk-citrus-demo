#pragma once

#include "SubVideoGL.h"

#ifdef _WIN32
#include <windows.h>
#endif

class Video
{
public:
	Video();
	~Video();

	bool Create(LPCWSTR title, bool fullscreen, unsigned int width, unsigned int height);
	void RenderBegin(float bg_r = .0f, float bg_g = .0f, float bg_b = .0f);
	void RenderEnd();

private:
	void setTitleFPS();
	void resizeWindow(unsigned int width, unsigned int height);

	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	LPCWSTR			m_title;
	unsigned int    m_width;
	unsigned int    m_height;
	bool            m_fullscreen;

	HDC     m_hDC;
	HWND    m_hWnd;

	HGLRC   m_hGLRC;
};