#pragma once

#include <stack>
#include "Matrix.h"

class TransformStack{
	std::stack<Matrix4f> stack;
public:

	TransformStack(){
		stack.push(Matrix4f::identity());
	}

	void apply(const Matrix4f& matrix){
		stack.top() = stack.top() * matrix;
	}

	void push(){
		stack.push(stack.top());
	}

	void pop(){
		stack.pop();
	}

	operator Matrix4f(){
		return stack.top();
	}

};



