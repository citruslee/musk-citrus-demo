﻿#pragma once
#include <glew.h>
#include <wglew.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include <stdlib.h>
#include <vector>
#include <cmath>
#include "Vector.h"
#include "Matrix.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

struct textVertex
{
	float pos[4];
	float normal[4]; // the average normal
	float uv[2];
	short numFaces;  // number of faces shared by the vertex
	long colora;     // ambient colour - change to a colour structure
	long colord;     // diffuse color    - change to a colour structure
	long colors;     // specular colour  - change to a colour structure
};

class Text
{
protected:
	//Vector3f size;
	int numTriangles = 12;
	struct textVertex * vtx = NULL;
	int sphereVBO;   // Vertex handle that contains interleaved positions and colors
	int triangleVBO; // Triangle handle that contains triangle indices
	int * ind = NULL;
	int numInd;
public:
	Vector4f materialAmbient;
	Vector4f materialDiffuse;
	Vector4f materialSpecular;
	GLuint texttex;

	Text(char *filename)
	{
		int w;
		int h;
		int comp;
		unsigned char* image = stbi_load(filename, &w, &h, &comp, STBI_rgb_alpha);

		if (image == nullptr)
		{
			throw(std::string("(ノ ゜Д゜)ノ ︵ ┻━┻"));
		}

		glGenTextures(1, &this->texttex);

		glBindTexture(GL_TEXTURE_2D, this->texttex);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		if (comp == 3)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		}
		else if (comp == 4)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		}
		glBindTexture(GL_TEXTURE_2D, 0);

		stbi_image_free(image);
		materialAmbient = Vector4f(0, 0, 0, 0);
		materialDiffuse = Vector4f(0, 0, 0, 0);
		materialSpecular = Vector4f(0, 0, 0, 0);

		int numVtx = 36;

		// allocate memory
		vtx = (struct textVertex *) malloc(sizeof(struct textVertex) * numVtx);
		if (vtx == NULL) {
			printf("Oops! An error occurred\n");
		}

		ind = (int *)malloc(sizeof(int) * numTriangles * 3);
		if (ind == NULL) {
			// error
			printf("Oops! An error occurred\n");
		}

		vtx[0].pos[0] = -1.0f;
		vtx[0].pos[1] = -1.0f;
		vtx[0].pos[2] = 0.0f;
		vtx[0].pos[3] = 1.0f;

		vtx[0].uv[0] = 0.0f;
		vtx[0].uv[1] = 0.0f;

		vtx[1].pos[0] = -1.0f;
		vtx[1].pos[1] = 1.0f;
		vtx[1].pos[2] = 0.0f;
		vtx[1].pos[3] = 1.0f;

		vtx[1].uv[0] = 0.0f;
		vtx[1].uv[1] = 1.0f;

		vtx[2].pos[0] = 1.0f;
		vtx[2].pos[1] = -1.0f;
		vtx[2].pos[2] = 0.0f;
		vtx[2].pos[3] = 1.0f;

		vtx[2].uv[0] = 1.0f;
		vtx[2].uv[1] = 0.0f;

		vtx[3].pos[0] = 1.0f;
		vtx[3].pos[1] = 1.0f;
		vtx[3].pos[2] = 0.0f;
		vtx[3].pos[3] = 1.0f;

		vtx[3].uv[0] = 1.0f;
		vtx[3].uv[1] = 1.0f;

		ind[0] = 1;
		ind[1] = 0;
		ind[2] = 2;

		ind[3] = 1;
		ind[4] = 2;
		ind[5] = 3;

		// *numVtx1 = numVtx;
		numInd = numTriangles * 3;

		glGenBuffers(1, (GLuint *)&sphereVBO);
		glBindBuffer(GL_ARRAY_BUFFER, sphereVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(struct textVertex)*numVtx, vtx, GL_STATIC_DRAW);
		glGenBuffers(1, (GLuint *)&triangleVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleVBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * numInd, ind, GL_STATIC_DRAW);
	}

	~Text()
	{
		std::cout << "Deleted sphere" << std::endl;
	}

	void draw(GLuint shaderProg)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);
		int positionLoc = glGetAttribLocation(shaderProg, "vPosition");
		int normalLoc = glGetAttribLocation(shaderProg, "vNormal");
		int texloc = glGetAttribLocation(shaderProg, "vUv");
		glEnableVertexAttribArray(positionLoc);
		glEnableVertexAttribArray(normalLoc);
		glEnableVertexAttribArray(texloc);
		glBindBuffer(GL_ARRAY_BUFFER, sphereVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleVBO);

		// Tells OpenGL how to walk through the two VBOs
		struct textVertex v;
		int relAddress = (char *)v.pos - (char *)&v;
		glVertexAttribPointer(positionLoc, 4, GL_FLOAT, GL_TRUE, sizeof(struct textVertex), (char*)NULL + relAddress);
		relAddress = (char *)v.normal - (char *)&v;
		glVertexAttribPointer(normalLoc, 4, GL_FLOAT, GL_TRUE, sizeof(struct textVertex), (char*)NULL + relAddress);
		relAddress = (char *)v.uv - (char *)&v;

		glVertexAttribPointer(texloc, 2, GL_FLOAT, GL_TRUE, sizeof(struct textVertex), (char*)NULL + relAddress);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		GLuint texID = glGetUniformLocation(shaderProg, "renderedTexture");
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texttex);
		// draw the triangles
		glDrawElements(GL_TRIANGLES, numTriangles * 3, GL_UNSIGNED_INT, (char*)NULL + 0);
		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
	}

};