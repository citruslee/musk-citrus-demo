#pragma once

#include <glew.h>

class Shader 
{
public:
	Shader(void);
	~Shader(void);

	char * readCode(char * fileName);
	int createShaderObj(char* fileName, int shaderType, GLuint *shaderid);

	// functions creates a shader program.  The two shader programs (vertex and fragment) were already compiled.
	int createShaderProgram(int vertShaderid, int fragShaderid, GLuint *shaderProgId);
	GLuint shaderProgramid;
	// creates a shader program from files vsFileName and fsFileName
	int createShaderProgram(char * vsFileName, char * fsFileName, unsigned int *shaderProgramid);
	int shaderStatus(void);
private:
	GLuint vertShaderid;
	GLuint fragShaderid;
};
