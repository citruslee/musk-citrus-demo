uniform sampler2D texture;

float rand(float x)
{
    return cos(sin(cos(x*1311.321)*1333.421)*2124.512);
}

void main()
{
    vec4 acc = vec4(0.0 ,0.0 ,0.0 ,0.0 );

    for (float i=-1.0; i<=1.0; i+=0.05)
    {
        //float rv = rand(i+dot(sin(gl_FragCoord.xy),vec2(12.0,3128.0)));
        //float f = i+rv*0.25;  
        float f = i; 
        vec2 wscale = vec2(80.0/800.0,80.0/600.0);
        vec2 lookup = gl_TexCoord[0].xy+vec2(f, 0.0)*wscale;;
        acc += texture2D(texture,lookup)*(1.0/40.0);
    }
    acc+=rand(dot(gl_FragCoord.xy,vec2(1.0,132.0))+dot(acc.xy,vec2(1.0,132.0)))*(1.0/256.0);
    acc.w=1.0;
    gl_FragColor = acc;
    //gl_FragColor = texture2D(texture ,gl_TexCoord[0].xy+vec2(sin(gl_TexCoord[0].y*frequency)*amplitude,sin(gl_TexCoord[0].x*frequency)*amplitude));
}
