uniform sampler2D texture ;
uniform float amplitude;
uniform float frequency;
void main()
{
    vec2 coord = gl_TexCoord[0].xy+vec2(sin(gl_TexCoord[0].y*frequency)*amplitude,sin(gl_TexCoord[0].x*frequency)*amplitude);
    vec4 c = texture2D(texture ,fract(coord));
    c.w = 1.0;
    gl_FragColor = mix(c,c*c,vec4(0.1,0.5,0.5,0.0));
}
