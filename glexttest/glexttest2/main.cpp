#undef UNICODE
#include <time.h>

#include "../resources/resources.h"


void glBindFramebuffer(gl::Framebuffer* x)
{
	if (x != 0)
	{	
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,x->hfb);
	}
	else
	{
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,0);	
	}
}

void glBindTexture(gl::Framebuffer* x)
{
	if (x == 0)
	{
		glBindTexture(GL_TEXTURE_2D,0);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D,x->htex);
	}
}

void glBindTexture(gl::Framebuffer* x,unsigned texture_unit)
{
	glActiveTexture(texture_unit);
	glBindTexture(x);
}

void qq()
{
	glBegin(GL_TRIANGLES);
	glColor3f(0.1,0.1,0.1);
	glVertex2f(0.0,1.3);
	glColor3f(0,0.1,0.25);
	glVertex2f(-1.0,-1.0);
	glColor3f(0,0.25,0.15);
	glVertex2f(+1.0,-1.0);
	glEnd();
}

gl::Program *cube_surface;
gl::Texture *tex0;
gl::Texture *tex1;
gl::Texture *tex2;
unsigned block_tex[4];

void render()
{
	//glBindProgram(0);
	gl::UseDefaultProgram();

	gl::TextureUnit.Disable();

	glEnable(GL_MULTISAMPLE_ARB);
	glClearColor(0.1,0.2,0.3,1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glScalef(0.1,0.1,0.1);
	for (int y=-10;y<=10;y++)
	for (int x=-10;x<=10;x++)
	{
		glPushMatrix();
		glTranslatef(x,-y,0);
		glRotatef(x*12+y*135+clock()*0.07+(1+sin(clock()*0.003))*20,0,0,1);
		glScalef(1.2,1.2,1.2);
		qq();
		glPopMatrix();
	}

	glDisable(GL_MULTISAMPLE_ARB);

		glEnable(GL_MULTISAMPLE_ARB);
	//glClearColor(0,0,0,1);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	gluPerspective(45.0,1.3,0.1,100.0);
	//glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClear(GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	
	cube_surface->UseProgram();

	int anint = GL_TEXTURE0;

	GLuint t0Location = cube_surface->UniformLocation("Texture0");
    GLuint t1Location = cube_surface->UniformLocation("Texture1");
	GLuint t2Location = cube_surface->UniformLocation("Texture2");

	/*
	cube_surface->Uniform(t0Location, 0);
    cube_surface->Uniform(t1Location, 1);
	cube_surface->Uniform(t2Location, 2);*/
	/*
	glActiveTexture(GL_TEXTURE0);
	glGetIntegerv(GL_ACTIVE_TEXTURE, &anint);
	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,block_tex[0]);

	glActiveTexture(GL_TEXTURE1);
	glGetIntegerv(GL_ACTIVE_TEXTURE, &anint);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,block_tex[1]);

	glActiveTexture(GL_TEXTURE2);
	glGetIntegerv(GL_ACTIVE_TEXTURE, &anint);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,block_tex[2]);

	glActiveTexture(GL_TEXTURE0);
	glGetIntegerv(GL_ACTIVE_TEXTURE, &anint);
	*/

	gl::TextureUnit[0].Bind(tex0,t0Location);
	gl::TextureUnit[1].Bind(tex1,t1Location);
	gl::TextureUnit[2].Bind(tex2,t2Location);

	float a[] ={10,0,10};
	glLightfv(GL_LIGHT0,GL_POSITION,a);

	glTranslatef(0,0,-5);
	glRotatef(clock()*0.01,0,0,1);
	glRotatef(clock()*0.021,0,1,0);
	glRotatef(clock()*0.031,1,0,0);
	//glScalef(0.7,0.7,1.0);
	/*glBegin(GL_TRIANGLES);
	glColor3f(1,0,0);
	glVertex2f(0,-1);
	glColor3f(0,1,0);
	glVertex2f(+1,+1);
	glColor3f(0,0,1);
	glVertex2f(-1,+1);
	glEnd();*/
	glColor3f(0.4,0.5,0.6);
	glCube(1.0);

	glDisable(GL_MULTISAMPLE_ARB);

}

void sheet()
{
	glBegin(GL_QUADS);
	glTexCoord2f(0,0); glVertex2f(0,0);
	glTexCoord2f(1,0); glVertex2f(1,0);
	glTexCoord2f(1,1); glVertex2f(1,1);
	glTexCoord2f(0,1); glVertex2f(0,1);
	glEnd();
}

void sheetouv(float u,float v)
{
	glBegin(GL_QUADS);
	glTexCoord2f(0+u,0+v); glVertex2f(0,0);
	glTexCoord2f(1+u,0+v); glVertex2f(1,0);
	glTexCoord2f(1+u,1+v); glVertex2f(1,1);
	glTexCoord2f(0+u,1+v); glVertex2f(0,1);
	glEnd();
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
    {
        case WM_CLOSE:
            DestroyWindow(hwnd);
        break;
        case WM_DESTROY:
            PostQuitMessage(0);
        break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

int main()
{
	int r;
	WNDCLASS wc;
	HWND hwnd;
	char* cn = "hehehe";
	memset(&wc,0,sizeof(WNDCLASS));
	wc.lpszClassName=cn;
	wc.lpfnWndProc=DefWindowProc;

	int res = RegisterClass(&wc);
	hwnd = CreateWindow(cn,"Wind",0,0,0,800,600,0,0,0,0);
	ShowWindow(hwnd,1);

	static  PIXELFORMATDESCRIPTOR pfd=                  // pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),                  // Size Of This Pixel Format Descriptor
		1,                              // Version Number
		PFD_DRAW_TO_WINDOW |                        // Format Must Support Window
		PFD_SUPPORT_OPENGL |                        // Format Must Support OpenGL
		PFD_DOUBLEBUFFER,                       // Must Support Double Buffering
		PFD_TYPE_RGBA,                          // Request An RGBA Format
		24,                               // Select Our Color Depth
		0, 0, 0, 0, 0, 0,                       // Color Bits Ignored
		0,                              // No Alpha Buffer
		0,                              // Shift Bit Ignored
		0,                              // No Accumulation Buffer
		0, 0, 0, 0,                         // Accumulation Bits Ignored
		16,                             // 16Bit Z-Buffer (Depth Buffer)
		0,                              // No Stencil Buffer
		0,                              // No Auxiliary Buffer
		PFD_MAIN_PLANE,                         // Main Drawing Layer
		0,                              // Reserved
		0, 0, 0                             // Layer Masks Ignored
	};

	HDC dc = GetDC(hwnd);

	int iPixelFormat = ChoosePixelFormat(dc, &pfd); 
	r = SetPixelFormat(dc,iPixelFormat,&pfd);

	HGLRC rc =  wglCreateContext(dc);
	wglMakeCurrent(dc,rc);

	//got context, get extensions
	if (!InitGlExtensions())
	{
		return 1;
	}

	int valid;
	int pf;
	unsigned numFormats;
	float fAttributes[] = {0,0};
	 int iAttributes[] = { WGL_DRAW_TO_WINDOW_ARB,GL_TRUE,
        WGL_SUPPORT_OPENGL_ARB,GL_TRUE,
        WGL_ACCELERATION_ARB,WGL_FULL_ACCELERATION_ARB,
        WGL_COLOR_BITS_ARB,24,
        WGL_ALPHA_BITS_ARB,8,
        WGL_DEPTH_BITS_ARB,16,
        WGL_STENCIL_BITS_ARB,0,
        WGL_DOUBLE_BUFFER_ARB,GL_TRUE,
        WGL_SAMPLE_BUFFERS_ARB,GL_TRUE,
        WGL_SAMPLES_ARB, 4 ,                        // Check For 4x Multisampling
        0,0};
 
    // First We Check To See If We Can Get A Pixel Format For 4 Samples
	 iAttributes[19] = 8; //8 samples
	valid = wglChoosePixelFormatARB(dc,iAttributes,fAttributes,1,&pf,&numFormats);
	if (!valid)
	{
		iAttributes[19] = 4; //4 samples
		valid = wglChoosePixelFormatARB(dc,iAttributes,fAttributes,1,&pf,&numFormats);
	}
	if (!valid)
	{
		iAttributes[19] = 2; //2 samples
		valid = wglChoosePixelFormatARB(dc,iAttributes,fAttributes,1,&pf,&numFormats);
	}
	if (!valid)
	{
		goto endofsetup;
	}


	wglDeleteContext(rc);
	
	DestroyWindow(hwnd);
	hwnd = CreateWindow(cn,"Wind",0,0,0,800,600,0,0,0,0);	
	ShowWindow(hwnd,1);
	dc=GetDC(hwnd);

//	iPixelFormat = ChoosePixelFormat(dc, &pfd); 	
	r = SetPixelFormat(dc,pf,&pfd);

	rc = wglCreateContext(dc);
	wglMakeCurrent(dc,rc);

endofsetup:
	
	
	/////// fbo
	/*
	unsigned tex,ColorBufferID;
	unsigned fb;

	//framebuffah
	glGenFramebuffersEXT(1, &fb);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);

	//color t
	glGenTextures(1,&tex);
	glBindTexture(GL_TEXTURE_2D,tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmapEXT(GL_TEXTURE_2D);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 800, 600, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);
	glFramebufferTexture2DEXT( GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex, 0 );
	//NULL means reserve texture memory, but texels are undefined*/

	gl::Framebuffer buffer(800,600);
	gl::Framebuffer buffer2(800,600);
	gl::Framebuffer buffer3(800,600);

	//multisample buffer
	
	
	
	//-------------------------
	//glGenRenderbuffersEXT(1, &depth_rb);
	//glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depth_rb);
	//glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, 256, 256);
	//-------------------------
	//Attach depth buffer to FBO
	//glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depth_rb);
	//-------------------------
	//Does the GPU support current FBO configuration?

	int status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
	{
		int derp = 5;
	}



	printf("%s\n",glGetString(GL_VENDOR));
	printf("%s\n",glGetString(GL_RENDERER));
	printf("%s\n",glGetString(GL_VERSION));
	printf("%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
	printf("%s\n",glGetString(GL_EXTENSIONS));


	gl::Program *nprg = new gl::Program(new gl::FragmentShader("test.frag"));
	int uamploc = glGetUniformLocation(nprg->id,"amplitude");
	int ufreloc = glGetUniformLocation(nprg->id,"frequency");

	gl::Program *blurx = new gl::Program(new gl::FragmentShader("blur_x.frag"));
	gl::Program *blury = new gl::Program(new gl::FragmentShader("blur_y.frag"));
	gl::Program *postprg = new gl::Program(new gl::FragmentShader("postprg.frag"));

	cube_surface = new gl::Program(new gl::FragmentShader("cube_surface.frag"),new gl::VertexShader("cube_surface.vert"));

	GLuint t0Location = glGetUniformLocation(cube_surface->id, "Texture0");
    GLuint t1Location = glGetUniformLocation(cube_surface->id, "Texture1");
	GLuint t2Location = glGetUniformLocation(cube_surface->id, "Texture2");

    glUniform1i(t0Location, 0);
    glUniform1i(t1Location, 1);
	glUniform1i(t2Location, 2);

	tex0 = new gl::Texture("bl_smooth.jpg");
	tex1 = new gl::Texture("brick_bump32.jpg");
	tex2 = new gl::Texture("basikblokdetail.jpg");

	glGenTextures(3,block_tex);
	glBindTexture(GL_TEXTURE_2D,block_tex[0]);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGBA,tex0->width,tex0->height,GL_RGB,GL_UNSIGNED_BYTE,tex0->imageData);
	glBindTexture(GL_TEXTURE_2D,block_tex[1]);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGBA,tex1->width,tex1->height,GL_RGB,GL_UNSIGNED_BYTE,tex1->imageData);
	glBindTexture(GL_TEXTURE_2D,block_tex[2]);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGBA,tex2->width,tex2->height,GL_RGB,GL_UNSIGNED_BYTE,tex2->imageData);

	float lt = 0;

	while (!GetAsyncKeyState(VK_ESCAPE))
	{
		//fb
		//glBindFramebuffer(&buffer);
		//glViewport( 0, 0, 800, 600 );

	//	render();
		float t = clock()*0.001;
		float dt = t-lt;
		lt = t;
		
		static float var_param_0=0,var_param_1=0;
		if (GetAsyncKeyState('Q')){var_param_0+=dt;}
		if (GetAsyncKeyState('A')){var_param_0-=dt;}
		if (GetAsyncKeyState('W')){var_param_1+=dt;}
		if (GetAsyncKeyState('S')){var_param_1-=dt;}

		//screen
		glBindProgram(0);
		
		glBindFramebuffer(0);
		glViewport( 0, 0, 800, 600 );

		glClearColor(0.1,0.4,0.3,1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		render();

		//done, let the post begin

		gl::TextureUnit.Disable();
		gl::TextureUnit[0].Enable();

		glEnable(GL_TEXTURE_2D);

		glBindTexture(&buffer);
		//int err = glGetError();


		glCopyTexImage2D(GL_TEXTURE_2D,0, GL_RGB8, 0,0,800,600,0);
	//	err = glGetError();
		
		buffer.GenerateMipmap();

		//glGenerateMipmapEXT(GL_TEXTURE_2D);
		
		//glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_LOD_BIAS,5.0);


		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glTranslatef(-1,-1,0);
		glScalef(2,2,2);

		glEnable(GL_BLEND);

		glBindTexture(&buffer);
		glBindFramebuffer(&buffer3);
		
		glBlendFunc(GL_ONE,GL_ZERO);

		glColor4f(1.00,1.00,1.00,1.0);
		
		glBindProgram(nprg);
		glUniform1f(uamploc,pow(2.0f,var_param_0));
		glUniform1f(ufreloc,pow(2.0f,var_param_1));

		sheetouv(0,0);

		glBindTexture(&buffer3);
		glBindFramebuffer(&buffer);

		//glBindProgram(0);
		glBindProgram(blurx);
		sheetouv(0,0);

		glBindTexture(&buffer);
		glBindFramebuffer(&buffer2);

		//glBindProgram(0);
		glBindProgram(blury);
		sheetouv(0,0);

		glBindTexture(&buffer3);
		glBindFramebuffer(&buffer2);

		//glBindProgram(0);
		glBindProgram(0);
		glBlendFunc(GL_ONE,GL_ONE);
		sheetouv(0,0);

		glBindTexture(&buffer2);
		glBindFramebuffer(0);

		glDisable(GL_BLEND);
		glBlendFunc(GL_ZERO,GL_ZERO);
		//glBindProgram(0);
		glBindProgram(postprg);
		sheetouv(0,0);
	

		/*
		glColor4f(0.525,0.525,0.525,1.0);
		sheetouv(1.0/800*5*5,0);
		sheetouv(0,1.0/600*5*5);
		sheetouv(-1.0/800*5*5.1,0);
		sheetouv(0,-1.0/600*5*5);

		glColor4f(0.25,0.25,0.25,1.0);
		sheetouv(2.0/800*5*5,0);
		sheetouv(0,2.0/600*5*5);
		sheetouv(-2.0/800*5*5.1,0);
		sheetouv(0,-2.0/600*5*5);
		*/
	/*	glBegin(GL_QUADS);
		glColor4f(2.0,1.0,1.0,1.0);
		glTexCoord2f(0,0); glVertex2f(0,0);
		glTexCoord2f(1,0); glVertex2f(1,0);
		glColor4f(2.0,1.0,1.0,0.0);
		glTexCoord2f(1,1); glVertex2f(1,1);
		glTexCoord2f(0,1); glVertex2f(0,1);
		glEnd();
		*/
		glColor4f(1.0,1.0,1.0,1.0);
		
		glDisable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		
		SwapBuffers(dc);

		MSG msg;
		while(PeekMessage(&msg, NULL, 0, 0, 1))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	r = glGetError();
	return 0;
}