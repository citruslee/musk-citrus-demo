uniform sampler2D texture;

float rand(float x)
{
    return cos(sin(cos(x*1311.321)*1333.421)*2124.512);
}


void main()
{
    vec4 c = texture2D(texture,gl_TexCoord[0].xy);

    float w = c.x*0.3+c.y*0.5+c.z*0.2;
    c = mix(c,vec4(w,w,w,1.0)*2.0,w*1.4-0.2);

    c+=rand(dot(gl_FragCoord.xy,vec2(1.0,132.0))+dot(c.xy,vec2(1.0,132.0)))*(1.0/256.0);

    gl_FragColor = c;

    
    //gl_FragColor = vec4(0,0,0,1.0);
}
