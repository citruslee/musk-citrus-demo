#pragma once
#undef UNICODE
#include <stdio.h>
#include "texturelib.h"

#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "wglext.h"
#include "myext.h"

typedef Texture tlibTexture;

//#undef Texture

namespace gl
{
	class Shader
	{
	public:
		unsigned id;
		char* source;
		char* source_file;
		int source_len;
		Shader(unsigned type)
		{
			source = 0;
			source_len =0;
			id = glCreateShader(type);
		}

		Shader(unsigned type, char* filename)
		{
			source = 0;
			source_len =0;
			id = glCreateShader(type);
			LoadSource(filename);
		}

		~Shader()
		{
			if (source!=0)
			{
				delete source;
				source = 0;
				source_len =0;
			}
		}

		void LoadSource(char* filename)
		{
			FILE *f = fopen(filename,"r");
			if (f==0)
			{
				MessageBox(0,filename,"File Read Error",MB_OK);
				return;
			}
			fseek(f,0,SEEK_END);
			int size = ftell(f);

			fseek(f,0,SEEK_SET);
			source = new char[size+1];
			source_len=size;
			int endp = fread(source,1,size,f);																																						
			source[endp]=0;
			fclose(f);
			glShaderSource(id,1,&source,&source_len);
			glCompileShader(id);
			char log[1024];
			glGetInfoLog(id,1024,0,log); if (log[0]) MessageBox(0,log,"SHADER error",MB_OK);
		}
	};

	class VertexShader : public Shader
	{
	public:
		VertexShader(char* filename) : Shader(GL_VERTEX_SHADER,filename)
		{
		
		}
	};

	class FragmentShader : public Shader
	{
	public:
		FragmentShader(char* filename) : Shader(GL_FRAGMENT_SHADER,filename)
		{
		
		}
	};

	class Program
	{
	public:
		unsigned id;

		Program()
		{
			id = glCreateProgram();
		}

		Program(Shader* s0, Shader* s1 = 0, Shader* s2 = 0, Shader* s3 = 0, Shader* s4 = 0, Shader* s5 = 0, Shader* s6 = 0, Shader* s7 = 0)
		{
			id = glCreateProgram();
			if (s0!=0) AttachShader(s0);
			if (s1!=0) AttachShader(s1);
			if (s2!=0) AttachShader(s2);
			if (s3!=0) AttachShader(s3);
			if (s4!=0) AttachShader(s4);
			if (s5!=0) AttachShader(s5);
			if (s6!=0) AttachShader(s6);
			if (s7!=0) AttachShader(s7);
			this->Link();
		}

		void AttachShader(Shader *target)
		{
			glAttachShader(id,target->id);
		}

		void Link()
		{
			glLinkProgram(id);
			char log[1024];
			glGetInfoLog(id,1024,0,log); if (log[0]) MessageBox(0,log,"Shader link error",MB_OK);
		}

		int UniformLocation(char* name)
		{
			return glGetUniformLocation(this->id,name);
		}

		void Uniform(int location,int value)
		{
			glUniform1i(location,value);
		}

		void Uniform(int location,float value)
		{
			glUniform1f(location,value);
		}

		void Uniform(char* name,int value)
		{
			glUniform1i(UniformLocation(name),value);
		}

		void Uniform(char* name,float value)
		{
			glUniform1f(UniformLocation(name),value);
		}

		void Use()
		{
			glUseProgram(id);
		}

		void UseProgram()
		{
			glUseProgram(id);
		}

	};

	class Framebuffer
	{
	public:
		unsigned hfb,hrbc,htex;

		Framebuffer()
		{
			hfb=0;
			hrbc=0;
			htex=0;
		}
		Framebuffer(unsigned width, unsigned height,bool bmipmap = 1, bool bdepth = 0)
		{
			glGenFramebuffersEXT(1, &hfb);
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, hfb);

			glGenTextures(1,&htex);
			glBindTexture(GL_TEXTURE_2D,htex);
			if (bmipmap)
			{	
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			}
			else
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			}

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);
			glFramebufferTexture2DEXT( GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, htex, 0 );
			glGenerateMipmapEXT(GL_TEXTURE_2D);
		}
		void GenerateMipmap()
		{
			glBindTexture(GL_TEXTURE_2D,this->htex);
			glGenerateMipmapEXT(GL_TEXTURE_2D);
		}

		void BindFramebuffer()
		{
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,this->hfb);	
		}

		void BindTexture()
		{
			glBindTexture(GL_TEXTURE_2D,this->htex);	
		}

	};

	class Texture : public tlibTexture
	{
	public:
		unsigned id;
		Texture() : tlibTexture()
		{
			id = 0;
		}
		Texture(char* filename) : tlibTexture(filename)
		{
			id = 0;
			this->Upload();
		}
		Texture(int width, int height) : tlibTexture(width,height)
		{
			id = 0;
		}
		void Upload()
		{
			if (id == 0)
			{
				glGenTextures(1,&id);
			}
			glBindTexture(GL_TEXTURE_2D,id);
			gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGBA,this->width,this->height,GL_RGB,GL_UNSIGNED_BYTE,this->imageData);
		}
		void BindTexture()
		{
			glBindTexture(GL_TEXTURE_2D,id);
		}
		void BindTexture(int TextureUnit)
		{
			glActiveTexture(GL_TEXTURE0+TextureUnit);
			glBindTexture(GL_TEXTURE_2D,id);
		}
	};

	class TextureUnitClass
	{
	public:
		int index;
		TextureUnitClass()
		{
			index = 0;
		}
		void MakeActive()
		{
			glActiveTexture(GL_TEXTURE0 + index);
		}
		void Enable()
		{
			glActiveTexture(GL_TEXTURE0 + index);
			glEnable(GL_TEXTURE_2D);
		}
		void Disable()
		{
			glActiveTexture(GL_TEXTURE0 + index);
			glDisable(GL_TEXTURE_2D);
		}
		void Bind(Texture* texture)
		{
			this->Enable();
			texture->BindTexture();
		}
		void Bind(int uniform_location)
		{
			glUniform1i(uniform_location,index);
		}
		void Bind(Texture* texture, int uniform_location)
		{
			Bind(texture); Bind(uniform_location);
		}

	};

	class TextureUnitContainerClass
	{
	public:
		static const int TUCOUNT = 16;
		TextureUnitClass *TU;
		
		TextureUnitContainerClass()
		{
			TU = new TextureUnitClass[TUCOUNT];
			for (int i=0; i<TUCOUNT; i++)
			{
				TU[i].index = i;
			}
		}

		~TextureUnitContainerClass()
		{
			delete[] TU;
		}

		TextureUnitClass& operator [] (int i)
		{
			TU[i].MakeActive();
			return TU[i];
		}

		void Disable()
		{
			for (int i=0; i<TUCOUNT; i++)
			{
				TU[i].Disable();
			}
		}

		void Enable()
		{
			for (int i=0; i<TUCOUNT; i++)
			{
				TU[i].Enable();
			}
		}

	};

	void UseDefaultFramebuffer()
	{
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,0);
	}

	void UseDefaultProgram()
	{
		glUseProgram(0);
	}

	extern TextureUnitContainerClass TextureUnit;
	//TextureUnitContainerClass TextureUnit;
};

void glCube(float scale=1.0)
{
	glBegin(GL_QUADS);
	
	glNormal3f(-1,0,0);
	glTexCoord2f(0,0); glVertex3d(-scale,-scale,-scale);
	glTexCoord2f(1,0); glVertex3d(-scale,+scale,-scale);
	glTexCoord2f(1,1); glVertex3d(-scale,+scale,+scale);
	glTexCoord2f(0,1); glVertex3d(-scale,-scale,+scale);

	glNormal3f(0,-1,0);
	glTexCoord2f(0,0); glVertex3d(-scale,-scale,-scale);
	glTexCoord2f(1,0); glVertex3d(+scale,-scale,-scale);
	glTexCoord2f(1,1); glVertex3d(+scale,-scale,+scale);
	glTexCoord2f(0,1); glVertex3d(-scale,-scale,+scale);

	glNormal3f(0,0,-1);
	glTexCoord2f(0,0); glVertex3d(-scale,-scale,-scale);
	glTexCoord2f(1,0); glVertex3d(+scale,-scale,-scale);
	glTexCoord2f(1,1); glVertex3d(+scale,+scale,-scale);
	glTexCoord2f(0,1); glVertex3d(-scale,+scale,-scale);

	glNormal3f(+1,0,0);
	glTexCoord2f(0,0); glVertex3d(+scale,-scale,-scale);
	glTexCoord2f(1,0); glVertex3d(+scale,+scale,-scale);
	glTexCoord2f(1,1); glVertex3d(+scale,+scale,+scale);
	glTexCoord2f(0,1); glVertex3d(+scale,-scale,+scale);

	glNormal3f(0,+1,0);
	glTexCoord2f(0,0); glVertex3d(-scale,+scale,-scale);
	glTexCoord2f(1,0); glVertex3d(+scale,+scale,-scale);
	glTexCoord2f(1,1); glVertex3d(+scale,+scale,+scale);
	glTexCoord2f(0,1); glVertex3d(-scale,+scale,+scale);

	glNormal3f(0,0,+1);
	glTexCoord2f(0,0); glVertex3d(-scale,-scale,+scale);
	glTexCoord2f(1,0); glVertex3d(+scale,-scale,+scale);
	glTexCoord2f(1,1); glVertex3d(+scale,+scale,+scale);
	glTexCoord2f(0,1); glVertex3d(-scale,+scale,+scale);

	glEnd();
}
