/* Basic texture functions for opengl.
 Planning a seperate I/O and seperate GL headers with resource manager. 
 A channel mixer too for files where color channels hold different data.

 Depends on
 > GL types, because of this windows types too; (GLubyte and GLuint mostly); can be easily removed.
 > stdio I/O functions

 Types: 
 > Texture - general texture type for usage with opengl;
 > Palette > general 256 color palette type for future indexed color support;

 Can:
 > load 24/32 bpp uncopressed/compressed TGA from file\
 > load some JPEG
 > save 24/32 bpp uncompressed TGA to file

 !!!!!!!!
 typedef struct {
   char  idlength;
   char  colourmaptype;
   char  datatypecode;
   short int colourmaporigin;
   short int colourmaplength;
   char  colourmapdepth;
   short int x_origin;
   short int y_origin;
   short width;
   short height;
   char  bitsperpixel;
   char  imagedescriptor; <<flipflopflags
} HEADER;

    X-origin (2 bytes): absolute coordinate of lower-left corner for displays where origin is at the lower left
    Y-origin (2 bytes): as for X-origin
    Image width (2 bytes): width in pixels
    Image height (2 bytes): height in pixels
    Pixel depth (1 byte): bits per pixel
!!!!Image descriptor (1 byte): bits 3-0 give the alpha channel depth, bits 5-4 give direction


*/

#pragma once

#ifdef BUILD_DLL

#define impex __declspec(dllexport)
#else
#define impex __declspec(dllimport)
#endif

//wat?

//#pragma comment(lib, "OpenGL32.lib")

//#include <Windows.h>
//#include <gl\GL.h>

//256-color palette, for index type;
struct Palette
{
	unsigned char r[256],g[256],b[256];
}; 

//general texture format; texture = any image in memory
class Texture
{
public:
	unsigned char *imageData;
	unsigned bpp;
	unsigned width;
	unsigned height;
	unsigned texID; //unused
	unsigned type; //Pixel format
	Palette *palette;
	Texture();
	Texture(char* filename);
	Texture(int width, int height);
	~Texture();
	bool Load(char* filename);
	bool Save(char* filename);
};

//gl pixel formats:
//GL_COLOR_INDEX,GL_STENCIL_INDEX,GL_DEPTH_COMPONENT,
//GL_RED,GL_GREEN,GL_BLUE,GL_ALPHA,GL_RGB,GL_RGBA,
//GL_LUMINANCE,GL_LUMINANCE_ALPHA

//should work, np;
bool impex TexDataInit(Texture *texture);
bool impex TexDataFree(Texture *texture);
// not tested yet
bool impex TexDataSetPixel(Texture *texture, unsigned x,unsigned y, unsigned char r,unsigned char g,unsigned char b,unsigned char a); 

bool impex TexLoadTGA(Texture *, char* filename);
bool impex TexSaveTGA(Texture *, char* filename);
bool impex TexLoadJPG(Texture *, char* filename);
bool impex TexLoadPNG(Texture *, char* filename);

	Texture::Texture()
	{
		this->imageData = 0; this->palette = 0;
	}
	Texture::Texture(char* filename)
	{
		Load(filename);
	}
	Texture::Texture(int width, int height)
	{
		bpp=32;
		this->width = width;
		this->height = height;
		type = 0x1908; //GL_RGBA
		TexDataInit(this);
	}
	Texture::~Texture()
	{
		TexDataFree(this);
	}
	bool Texture::Load(char* filename)
	{
		char* cp = filename;
		while (*cp) cp++;
		cp-= 4;
		switch (*(unsigned*)(cp)) //last 4 chars of filename
		{	
 		case 'gpj.': //.jpg
			return TexLoadJPG(this,filename);
		case 'gnp.': //.png
			return TexLoadPNG(this,filename);
		case 'agt.': //.tga
			return TexLoadTGA(this,filename);
		default:
			this->imageData = 0; this->palette = 0;
			return 0;
		}
	}
	bool Texture::Save(char* filename)
	{
		return TexSaveTGA(this,filename);
	}
