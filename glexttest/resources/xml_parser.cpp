// a fast hack, to help parse some collada files

#include <stdio.h>
#include <string.h>

char readbuffer[4096];
int bread;
char *p=readbuffer,*q=p;
int depth=0,length=0;
char *attrib;
FILE *f;

char* xml_get_read_buffer()
{
	return readbuffer;
}

FILE* xml_get_fileptr()
{
	return f;
}

void xml_begin(FILE *file)
{
	f=file;
	bread=1;
	depth=0;
	attrib=0;
}
void xml_fclose()
{
	fclose(f);
}

int xml_get_depth()
{
	return depth;
}
void xml_tag_get_next_attribute(char* tag, char* name_buffer, char* value_buffer)
{
	if (attrib == 0) 
	{
		attrib = tag+1;
		while (*attrib !=' ' && *attrib !='>') attrib++;
	}
	
	while (*attrib ==' ') attrib++;

	if (*attrib == '>' || *attrib == 0 || *attrib=='/' || *attrib=='?')
	{
		*name_buffer=*value_buffer=0;
		return;
	}

	while (*attrib !='=' && *attrib !=' ') *(name_buffer++) = *(attrib++);
	*name_buffer=0;

	while (*attrib != '\"' && *attrib != '\'') attrib++;
	attrib++;
	while (*attrib != '\"' && *attrib != '\'') *(value_buffer++) = *(attrib++);
	*value_buffer=0;
	attrib++;



}

void xml_tag_get_name(char* tag,char* name_buffer)
{
	char* si = tag+1;
	char* di = name_buffer;

	if (*si == '/') si++;
	if (*si == '?') si++;
	if (*si == '!') si++;
	while (*si ==' ') si++;

	while (*si != ' ' && *si!='>' && *si!='/') *(di++) = *(si++);
	*di=0;
}

void xml_get_tag(char* tag)
{
	attrib=0;
	int done=0;
	//while (!done)
	reset:
	{
		if (bread)
		{
 			fgets(readbuffer,4096,f);
			p=q=readbuffer;
			bread=0;
		}
	    while (*p!='<' && *p!=0) 
		{
			p++;
		}
		if (*p==0)
		{
			if (feof(f))
			{
				tag[0]=0;
				return;
			}
			else
			{
				bread=1;
				goto reset;
			}
		}
		q=p;
		while (*q!='>') 
		{
			q++;
		}
		q++;
		length=q-p;
		memcpy(tag,p,length);
		tag[length]=0;
		p=q;

		if (tag[1]=='/') depth--;

		if(tag[length-2]=='/' || tag[1]=='/') ;
		else if(tag[1]!='?' && tag[1]!='!') depth++;

		//int i=depth; while (i--) printf("  ");
		//printf("%s\n",tag);

		
		
	}
}