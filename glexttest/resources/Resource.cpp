#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "Resource.h"
#include "ResourceTypes.h"
#include <typeinfo>
/*
class Resource
{
public:
	virtual int Load();
	virtual int Unload();
	int Size;
	void* Data;
	char* Name;
	char* Path;
	Resource();
};
*/
Resource::Resource()
{
	Data=0;
	Name=0;
	Path=0;
}
Resource::~Resource()
{
	if (Data) free(Data);
	if (Name) free(Name);
	if (Path) free(Path);
}
int Resource::Load()
{
	assert(0);
	return 1;
}
int Resource::Unload()
{
	assert(0);
	return 1;
}

/*
class ResourceTable
{
public:
	Resource* Find(const char*);
	Resource* Fetch(const char*);//find res, load if needed,

	int Error;

	Resource** List; //list of pointers
	int ListCapacity;
	int ListSize;
	ResourceTable();
	~ResourceTable();
};
*/

ResourceTable::ResourceTable()
{
	ListCapacity = 0;
	ListSize = 0;
	List = 0;
	Error = 0;
}
ResourceTable::~ResourceTable()
{
	for (int i=0; i<ListSize; i++)
	{
		Resource *p = *(List+i);
		delete p;
	}
	if (List)
	{
		free(List);
	}
}
Resource* ResourceTable::Find(char* filename)
{
	for (int i=0; i<ListSize; i++)
	{
		Resource *p = *(List+i);
		if (strcmp(filename,p->Path)==0)
		{
			return p;
		}
	}
	return 0;
}
Resource* ResourceTable::Fetch(char* filename)
{
	if (!filename)
	{
		return 0;
	}
	if (Resource* Found = this->Find(filename))
	{
		return Found;
	}

	if (ListSize >= ListCapacity)
	{
		ListCapacity = ListCapacity*2+1;
		Resource** n = (Resource**) malloc(sizeof(Resource**) * ListCapacity*2+1);
		memcpy(n,List,sizeof(Resource**) * ListSize);
		if (List)
			free(List);
		List=n;
	}

	char* p = filename;
	while (*p != 0) p++;
	while (*p != '.' && p != filename) p--;

	if (p == filename)
	{
		//error
		return 0;
	}

	int size = strlen(filename);
	char* fncpy = (char*) malloc(size+1);
	strcpy(fncpy,filename);
	fncpy[size]=0;
	Resource *nr = 0;

	if (strcmp(p,".png")==0)
	{
		//png, texture
		nr = new RTexturePNG;
		nr->Path=fncpy;
		nr->Load();
		*(List+ListSize) = nr;
		ListSize++;
		return nr;
	}
	if (strcmp(p,".jpg")==0)
	{
		//jpg, texture
		nr = new RTextureJPG;
		nr->Path=fncpy;
		nr->Load();
		*(List+ListSize) = nr;
		ListSize++;
		return nr;
	}
	if (strcmp(p,".tga")==0)
	{
		//tga, texture
		nr = new RTextureTGA;
		nr->Path=fncpy;
		nr->Load();
		*(List+ListSize) = nr;
		ListSize++;
		return nr;
	}
	if (strcmp(p,".fx")==0)
	{
		//shader program
		//load .fs + .vs
		nr = new RShadingProgram;
		nr->Path=fncpy;
		nr->Load();
		*(List+ListSize) = nr;
		ListSize++;
		return nr;
	}
	if (strcmp(p,".dae")==0)
	{
		//load the collada scene
		nr = new RColladaScene;
		nr->Path=fncpy;
		nr->Load();
		*(List+ListSize) = nr;
		ListSize++;
		return nr;
	}
	if (strcmp(p,".mp3")==0)
	{
		//audio
		nr = new RAudioStream;
		nr->Path=fncpy;
		nr->Load();
		*(List+ListSize) = nr;
		ListSize++;
		return nr;
	}
	if (strcmp(p,".ogg")==0)
	{
		//audio
		nr = new RAudioStream;
		nr->Path=fncpy;
		nr->Load();
		*(List+ListSize) = nr;
		ListSize++;
		return nr;
	}
	if (strcmp(p,".wav")==0)
	{
		//audio
		nr = new RAudioStream;
		nr->Path=fncpy;
		nr->Load();
		*(List+ListSize) = nr;
		ListSize++;
		return nr;
	}
	return 0;
}

void* ResourceTable::Get(char* filename)
{
	if (!filename)
	{
		return 0;
	}
	if (Resource* Found = this->Find(filename))
	{
		if (typeid(Found) == typeid(RTexture))
		{
			RTexture* t = ((RTexture*)Found);
			return t->gltex;
		}
		if (typeid(Found) == typeid(RShader))
		{
			RShader* t = ((RShader*)Found);
			return t->glshader;
		}
		
	}

	if (ListSize >= ListCapacity)
	{
		ListCapacity = ListCapacity*2+1;
		Resource** n = (Resource**) malloc(sizeof(Resource**) * ListCapacity*2+1);
		memcpy(n,List,sizeof(Resource**) * ListSize);
		if (List)
			free(List);
		List=n;
	}

	char* p = filename;
	while (*p != 0) p++;
	//while (*p != '.' && p != filename) p--;
	unsigned filetype = *((unsigned*)p-4);
	if (p <= filename)
	{
		//error
		return 0;
	}

	int size = strlen(filename);
	char* fncpy = (char*) malloc(size+1);
	strcpy(fncpy,filename);
	fncpy[size]=0;
	Resource *nr = 0;

	switch(filetype)
	{
	case 'gnp.'://*.png
	case 'gpj.'://*.jpg 
	case 'gepj'://*jpeg
	case 'agt.'://*.tga
		{
			RTexture *t = new RTexture;
			nr = t;
			nr->Path=fncpy;
			nr->Load();
			*(List+ListSize) = nr;
			ListSize++;
			return t->gltex;
			break;
		}
	case 'garf'://*frag
	case 'trev'://*vert
		{
			RShader *t = new RShader;
			nr = t;
			nr->Path=fncpy;
			nr->Load();
			*(List+ListSize) = nr;
			ListSize++;
			return t->glshader;
			break;
		}
	case 'ead.'://*.dae
		{
			nr = new RColladaScene;
			nr->Path=fncpy;
			nr->Load();
			*(List+ListSize) = nr;
			ListSize++;
			return nr;
		}
	case '3pm.'://*.mp3
	case 'ggo.'://*.ogg
	case 'vaw.'://*.wav
		{
			//audio
			nr = new RAudioStream;
			nr->Path=fncpy;
			nr->Load();
			*(List+ListSize) = nr;
			ListSize++;
			return nr;
		}
	}
	return 0;
}

ResourceTable GRT;
