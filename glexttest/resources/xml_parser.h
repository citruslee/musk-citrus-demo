#pragma once

#include <stdio.h>

void xml_begin(FILE *file);
void xml_fclose();
int xml_get_depth();
void xml_tag_get_next_attribute(char* tag, char* name_buffer, char* value_buffer);
void xml_tag_get_name(char* tag,char* name_buffer);
void xml_get_tag(char* tag);
char* xml_get_read_buffer();
FILE* xml_get_fileptr();