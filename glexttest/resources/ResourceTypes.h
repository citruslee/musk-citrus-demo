#pragma once
#include "MinColladaLoader.h"
#include "texturelib.h"
#include "Resource.h"
#include "globjects.h"

class RTexture: public Resource
{
public:
	virtual int Load();
	virtual int Unload();
	unsigned glid;
	Texture* tex;
	gl::Texture* gltex;
	RTexture();
	~RTexture();
};

typedef RTexture RTexturePNG;
typedef RTexture RTextureTGA;
typedef RTexture RTextureJPG;
/*
class RTexturePNG: public RTexture
{
public:
	//virtual int Load();
	//virtual Unload();
};

class RTextureTGA: public RTexture
{
public:
	//virtual int Load();
};

class RTextureJPG: public RTexture
{
public:
	//virtual int Load();
};*/

class RShadingProgram: public Resource
{
public:
	unsigned glid;
	unsigned vsid;
	unsigned fsid;
	char* vssource;
	char* fssource;
	char* fs_path;
	char* vs_path;
	gl::Program* glprg;
	gl::FragmentShader* glfs;
	gl::VertexShader* glvs;
	RShadingProgram();
	virtual int Load();
};

class RShader:public Resource
{
public:
	int type;
	gl::Shader *glshader;
	RShader();
	virtual int Load();
};

class RColladaScene: public Resource
{
public:
	visual_scene* scene;
	scene_object* camera;
	scene_object* camera_target;

	collada_data_table* local_cdt;

	scene_object* FindSceneObject(const char* name);
	virtual int Load();
};

typedef RColladaScene ColladaScene;

class RAudioStream: public Resource
{
public:
	float Panning;
	float Volume;
	float BPS;
	unsigned bhandle;
	float lst;

	RAudioStream()
	{
		Panning = 0.0;
		Volume = 1.0f;
		BPS = 1.0f;
		bhandle = 0;
		lst = 0.0;
	}
	void Play();
	void Stop();

	float SyncToPlay(float t);

	virtual int Load();
};

typedef RAudioStream AudioStream;

void MeshRender(const mesh* mm);
void MeshDisplay(const mesh* mm);
void SceneObjectApplyTransform(const scene_object* cso);
void SceneObjectApplyAnim(scene_object* cso, const float t);
void glBindTexture(RTexture*);
void glBindProgram(RShadingProgram*);