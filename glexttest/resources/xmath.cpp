/* math.h extesion with vector, matrixes and transformations; 
 Planned: non-linear interpolations;
 Depends on math.h;
 Adds a new vector type;
 Adds a new matrix type;

 Lots of vector operators:
 ==,!= comparison
 +,-,+=,-= addition subtraction
 +,-,*,/ add sub or multiply with floats
 * dot product
 ^ cross product
 ~ vector size
 ! converts to unit vector

 Functions: 
 > vsize, vnorm, vnormal, vdist, vdistance, vcosa, vcosangle, vangl, vangle; 
 vnorm = !, vsize = ~, vdist = ~(a-b);
 > lerp(A,B,f); linearly interpolates 2 floats/vectors with a float as 3rd parameter [0.0-1.0]

 You can get the normal of a polygon with: !((A-B)^(C-B))

 Matrixes type matrix
 Functions:
 m_identity,m_translate,m_scale,m_rotate,m_mul,m_transform
 Operators:
 matrix * matrix multiplication 
 vector * matrix transformation
*/

#include "xmath.h"
/*
inline xmfloatt todeg(xmfloatt a) { return a*57.295779513082320876798154814105f; }
inline xmfloatt torad(xmfloatt a) { return a*0.017453292519943295769236907684886f; }
*/
bool operator == (vector a, vector b)	{ return ((a.x==b.x)&&(a.y==b.y)&&(a.z==b.z)); }
bool operator != (vector a, vector b)	{ return !(a==b); }
vector operator - (vector a)			{ a.x=-a.x; a.y=-a.y; a.z=-a.z; return a; }
/*
inline void operator += (vector &a,vector &b)	{ a.x+=b.x; a.y+=b.y; a.z+=b.z; }
inline void operator -= (vector &a,vector &b)	{ a.x-=b.x; a.y-=b.y; a.z-=b.z; }
inline vector operator + (vector a, vector &b)	{ a+=b; return a; }
inline vector operator - (vector a, vector &b)	{ a-=b; return a; }
inline xmfloatt operator * (vector &a, vector &b)	{ return a.x*b.x+a.y*b.y+a.z*b.z; }
inline vector operator ^ (vector &a, vector &b)	{ vector c; c.x=a.y*b.z-b.y*a.z; c.y=a.z*b.x-b.z*a.x; c.z=a.x*b.y-b.x*a.y; return c;}

inline void operator += (vector &a,xmfloatt &b)	{ a.x+=b; a.y+=b; a.z+=b; }
inline void operator -= (vector &a,xmfloatt &b)	{ a.x-=b; a.y-=b; a.z-=b; }
inline void operator *= (vector &a,xmfloatt &b)	{ a.x*=b; a.y*=b; a.z*=b; }
inline void operator /= (vector &a,xmfloatt &b)	{ a.x/=b; a.y/=b; a.z/=b; }
inline vector operator + (vector a, xmfloatt &b)	{ a+=b; return a; }
inline vector operator + (xmfloatt &b, vector a)	{ a+=b; return a; }
inline vector operator - (vector a, xmfloatt &b)	{ a-=b; return a; }
inline vector operator * (vector a, xmfloatt &b)	{ a*=b; return a; }
inline vector operator * (xmfloatt &b, vector a)	{ a*=b; return a; }
inline vector operator / (vector a, xmfloatt b)	{ a/=b; return a; }

inline xmfloatt vsize (vector &a)					{ return sqrt(a.x*a.x+a.y*a.y+a.z*a.z); }
inline xmfloatt operator ~(vector &a)				{ return vsize(a); }
inline xmfloatt vdistance (vector &a, vector &b)	{ return ~(a-b); } 
inline vector vnormal (vector &a)				{ return a/(~a); } 
inline vector operator !(vector &a)				{ return vnorm(a); }
inline xmfloatt vcosangle (vector &a, vector &b ) { return !a*!b; }
inline xmfloatt vangle (vector &a, vector &b )    { return acos(vcosa(a,b)); }
inline vector v(xmfloatt a,xmfloatt b, xmfloatt c)	{ vector d; d.x=a; d.y=b; d.z=c; return d; }
inline vector v(xmfloatt *a)						{vector b; b.x=a[0]; b.y=a[1]; b.z=a[2];}
inline vector lerp(vector &a, vector &b, xmfloatt c) {return a+c*(b-a); }
inline xmfloatt lerp(xmfloatt &a, xmfloatt &b, xmfloatt c) {return a+c*(b-a); }
*/
void m_identity(matrix  &a)
{
	a.m[0][0]=1.0; a.m[0][1]=0.0; a.m[0][2]=0.0; a.m[0][3]=0.0; 
	a.m[1][0]=0.0; a.m[1][1]=1.0; a.m[1][2]=0.0; a.m[1][3]=0.0; 
	a.m[2][0]=0.0; a.m[2][1]=0.0; a.m[2][2]=1.0; a.m[2][3]=0.0; 
	a.m[3][0]=0.0; a.m[3][1]=0.0; a.m[3][2]=0.0; a.m[3][3]=1.0; 
}
matrix m_identity()
{
	matrix c;
	m_identity(c);
	return c;
}

void m_mul(matrix &a,matrix &b, matrix &c)
{
	for (int x=0;x<4;x++)
	for (int y=0;y<4;y++)
	{
		c.m[y][x]=0.0;
		for (int z=0;z<4;z++)
		{
			c.m[y][x]+=a.m[y][z]*b.m[z][x];
		}
	}
}
matrix operator * (matrix &a,matrix &b)
{
	matrix c;
	m_mul(a,b,c);
	return c;
}
void operator *= (matrix &a,matrix &b)
{
	a=a*b;
}

void m_translate(matrix &a, xmfloatt x, xmfloatt y, xmfloatt z)   
{a.m[3][0]+=x; a.m[3][1]+=y; a.m[3][2]+=z;}
void m_translate(matrix &a, vector &b)   
{a.m[3][0]+=b.x; a.m[3][1]+=b.y; a.m[3][2]+=b.z;}
matrix m_translate(xmfloatt x, xmfloatt y, xmfloatt z)
{matrix c;m_identity(c); m_translate(c,x,y,z); return c;}
matrix m_translate(vector &b)
{matrix c;m_identity(c); m_translate(c,b); return c;}

void m_scale(matrix &a, xmfloatt s)
{
	a.m[0][0]*=s;a.m[1][0]*=s;a.m[2][0]*=s;a.m[3][0]*=s;
	a.m[0][1]*=s;a.m[1][1]*=s;a.m[2][1]*=s;a.m[3][1]*=s;
	a.m[0][2]*=s;a.m[1][2]*=s;a.m[2][2]*=s;a.m[3][2]*=s;
}
void m_scale(matrix &a, xmfloatt x, xmfloatt y, xmfloatt z)
{
	a.m[0][0]*=x;a.m[1][0]*=x;a.m[2][0]*=x;a.m[3][0]*=x;
	a.m[0][1]*=y;a.m[1][1]*=y;a.m[2][1]*=y;a.m[3][1]*=y;
	a.m[0][2]*=z;a.m[1][2]*=z;a.m[2][2]*=z;a.m[3][2]*=z;
}
void m_scale(matrix &a, vector &b)
{
	a.m[0][0]*=b.x;a.m[1][0]*=b.x;a.m[2][0]*=b.x;a.m[3][0]*=b.x;
	a.m[0][1]*=b.y;a.m[1][1]*=b.y;a.m[2][1]*=b.y;a.m[3][1]*=b.y;
	a.m[0][2]*=b.z;a.m[1][2]*=b.z;a.m[2][2]*=b.z;a.m[3][2]*=b.z;
}
matrix m_scale(xmfloatt s)
{
	matrix c=m_identity();
	m_scale(c,s);
	return c;
}
matrix m_scale(xmfloatt x,xmfloatt y, xmfloatt z)
{
	matrix c=m_identity();
	m_scale(c,x,y,z);
	return c;
}
matrix m_scale(vector &b)
{
	matrix c=m_identity();
	m_scale(c,b);
	return c;
}

void m_rotate_x(matrix &a, xmfloatt q)
{
	matrix c=m_identity();
	xmfloatt sinq = sin(q), cosq = cos(q);
	c.m[1][1]=cosq; c.m[1][2]=-sinq; c.m[2][1]=sinq; c.m[2][2]=cosq;
	a=a*c;
}
void m_rotate_y(matrix &a, xmfloatt q)
{
	matrix c=m_identity();
	xmfloatt sinq = sin(q), cosq = cos(q);
	c.m[0][0]=cosq; c.m[0][2]=sinq; c.m[2][0]=-sinq; c.m[2][2]=cosq;
	a=a*c;
}
void m_rotate_z(matrix &a, xmfloatt q)
{
	matrix c=m_identity();
	xmfloatt sinq = sin(q), cosq = cos(q);
	c.m[0][0]=cosq; c.m[0][1]=-sinq; c.m[1][0]=sinq; c.m[1][1]=cosq;
	a=a*c;
}
matrix m_rotate_x(xmfloatt q)
{
	matrix c=m_identity();
	xmfloatt sinq = sin(q), cosq = cos(q);
	c.m[1][1]=cosq; c.m[1][2]=-sinq; c.m[2][1]=sinq; c.m[2][2]=cosq;
	return c;
}
matrix m_rotate_y(xmfloatt q)
{
	matrix c=m_identity();
	xmfloatt sinq = sin(q), cosq = cos(q);
	c.m[0][0]=cosq; c.m[0][2]=sinq; c.m[2][0]=-sinq; c.m[2][2]=cosq;
	return c;
}
matrix m_rotate_z(xmfloatt q)
{
	matrix c=m_identity();
	xmfloatt sinq = sin(q), cosq = cos(q);
	c.m[0][0]=cosq; c.m[0][1]=-sinq; c.m[1][0]=sinq; c.m[1][1]=cosq;
	return c;
}

void m_rotate(matrix &a,xmfloatt q,xmfloatt x, xmfloatt y, xmfloatt z)
{
	a = m_identity();
	xmfloatt sinq = sin(q), cosq = cos(q);
	a.m[0][0]=cosq + x*x*(1-cosq);
	a.m[1][1]=cosq + y*y*(1-cosq);
	a.m[2][2]=cosq + z*z*(1-cosq);

	a.m[1][0]=y*x*(1 - cosq) + z*sinq;
	a.m[2][0]=z*x*(1 - cosq) - y*sinq;
	a.m[2][1]=z*y*(1 - cosq) + x*sinq;

	a.m[0][1]=x*y*(1 - cosq) - z*sinq;
	a.m[0][2]=x*z*(1 - cosq) + y*sinq;
	a.m[1][2]=y*z*(1 - cosq) - x*sinq;
}
void m_rotate(matrix &a,xmfloatt q, vector &b)
{
	m_rotate(a,q,b.x,b.y,b.z);
}
matrix m_rotate(xmfloatt q, xmfloatt x, xmfloatt y, xmfloatt z)
{
	matrix c; m_rotate(c,q,x,y,z); return c;
}
matrix m_rotate(float q, vector &b)
{
	matrix c; m_rotate(c,q,b.x,b.y,b.z); return c;
}

void m_transform(vector &a, matrix &b)
{
	vector c=a;
	a.x=c.x*b.m[0][0]+c.y*b.m[1][0]+c.z*b.m[2][0]+b.m[3][0];
	a.y=c.x*b.m[0][1]+c.y*b.m[1][1]+c.z*b.m[2][1]+b.m[3][1];
	a.z=c.x*b.m[0][2]+c.y*b.m[1][2]+c.z*b.m[2][2]+b.m[3][2];                                              
}
vector operator *(vector &c, matrix &b)
{
	vector a;
	//m_identity(a);
	a.x=c.x*b.m[0][0]+c.y*b.m[1][0]+c.z*b.m[2][0]+b.m[3][0];
	a.y=c.x*b.m[0][1]+c.y*b.m[1][1]+c.z*b.m[2][1]+b.m[3][1];
	a.z=c.x*b.m[0][2]+c.y*b.m[1][2]+c.z*b.m[2][2]+b.m[3][2];  
	return a;
}
void operator *= (vector &c, matrix &b)
{
	c=c*b;
}