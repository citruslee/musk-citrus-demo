#pragma once

struct mesh
{
	char* name;
	int triangle_count;
	int* triangle_list; //{v0 n0 uv0 v1 n1 uv1 v2 n2 uv2}
	float* vertex_list; //{x y z}
	float* normal_list; //{x y z}
	float* uv_list;// {u v}
	unsigned glid;
	unsigned gltexid0;
	unsigned gltexid1;
	unsigned gltexid2;
	unsigned gltexid3;
};

struct anim
{
	char* name;
	int count;
	float* input;
	float* output;
	float* itangent;
	float* otangent;
};

struct scene_object
{
	char* name;
	mesh* model;
	scene_object* pivot;
	float x,y,z,jox,joy,joz,xr,yr,zr,xs,ys,zs; 
	anim* anim_group[6]; //x y z xr, yr, zr
};

struct visual_scene
{
	int scene_object_count,scene_object_capacity;
	scene_object** scene_object_plist; //ptrlist to objects in the scene
	char* name;
	char* file;
};

struct collada_data_table
{
	int mesh_count,mesh_capacity;
	mesh* mesh_list;
	int anim_count,anim_capacity;
	anim* anim_list;
	int scene_object_count,scene_object_capacity;
	scene_object* scene_object_list;
	int visual_scene_count, visual_scene_capacity;
	visual_scene* visual_scene_list;
};

void load_collada(collada_data_table* cdat, char* filename);
