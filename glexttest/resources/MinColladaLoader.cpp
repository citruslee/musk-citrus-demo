#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "xml_parser.h"
#include <assert.h>
#include "MinColladaLoader.h"

void readvisualscenes(collada_data_table* cdat, char* filename)
{
	char tag[1024],name[1024],an[1024],av[1024];

	bool bReadingVisualScene=false;
	bool bReadingNode=false;
	bool bReadingPivot=false;

	scene_object* pivot;
	scene_object* cso;
	scene_object** cvso;
	visual_scene* cvs;

	while (true)
	{
		xml_get_tag(tag);
		xml_tag_get_name(tag,name);

		if (tag[1] != '/')
		{
			if (strcmp(name,"visual_scene")==0 && !bReadingVisualScene)
			{
				bReadingVisualScene=true;

				assert(cdat->visual_scene_count < cdat->visual_scene_capacity);
				cvs=cdat->visual_scene_list + cdat->visual_scene_count;
				cdat->visual_scene_count++;

				xml_tag_get_next_attribute(tag,an,av);
				cvs->name = (char*) malloc(strlen(av)+1);
				strcpy(cvs->name, av);

				FILE *f = xml_get_fileptr();
				cvs->file= (char*) malloc(strlen(filename)+1);
				strcpy(cvs->file, filename);

				cvs->scene_object_plist = (scene_object**) malloc(20*sizeof(scene_object*));
				cvs->scene_object_count=0;
				cvs->scene_object_capacity=20;

				continue;
			}
			if (strcmp(name,"node")==0 && bReadingVisualScene && bReadingNode)
			{
				bReadingPivot=true;
				pivot = (scene_object*) malloc(sizeof(scene_object));
				memset(pivot,0,sizeof(pivot));
				cso->pivot=pivot;
				pivot->jox=pivot->joy=pivot->joz=0;
				pivot->x=pivot->y=pivot->z=0;
				pivot->xr=pivot->yr=pivot->zr=0;
				pivot->xs=pivot->ys=pivot->zs=1;
				pivot->pivot=0;
				pivot->model=0;
				continue;
			}
			if (strcmp(name,"node")==0 && bReadingVisualScene && !bReadingNode)
			{
				bReadingNode=true;
				assert(cvs->scene_object_count < cvs->scene_object_capacity);
				
				cvso=(cvs->scene_object_plist + cvs->scene_object_count);
				cvs->scene_object_count++;

				xml_tag_get_next_attribute(tag,an,av);

				scene_object* dso=0;
				char *si, *di;
				bool bFound=false;

				for (int i=0; i<cdat->scene_object_count; i++)
				{
					dso = cdat->scene_object_list + i;
					if (strcmp(av,dso->name)==0)
					{
						*cvso = dso;
						cso = dso;
						bFound=true;
						break;
					}
				}
				if (!bFound)
				{
						assert(cdat->scene_object_count < cdat->scene_object_capacity);
						cso = cdat->scene_object_list + cdat->scene_object_count;
						cdat->scene_object_count++;

						*cvso = cso;
				
						char* tn = (char*)malloc(strlen(av)+1);
						strcpy(tn,av);
						cso->name=tn;
						mesh* i = cdat->mesh_list;
						cso->x=cso->y=cso->z=cso->xr=cso->yr=cso->zr=0.0;
						cso->xs=cso->ys=cso->zs=1.0;
						cso->joz=cso->joy=cso->jox=0.0;
						cso->anim_group[0]=cso->anim_group[1]=cso->anim_group[2]=
						cso->anim_group[3]=cso->anim_group[4]=cso->anim_group[5]=0;
						cso->model=0;
						cso->pivot=0;
						//break;
					}
				continue;
			}
			if (strcmp(name,"translate")==0 && bReadingNode)
			{
				char* b = xml_get_read_buffer();
				while (*b!='>') b++; 
				b++;
				if (bReadingPivot)
				{
					sscanf(b,"%f%f%f",&pivot->x,&pivot->y,&pivot->z);
				}
				else
				{
					sscanf(b,"%f%f%f",&cso->x,&cso->y,&cso->z);
				}
				continue;
			}
			if (strcmp(name,"scale")==0 && bReadingNode)
			{
				char* b = xml_get_read_buffer(); while (*b!='>') b++; b++;
				if (bReadingPivot)
				{
					sscanf(b,"%f%f%f",&pivot->xs,&pivot->ys,&pivot->zs); 
				}
				else
				{
					sscanf(b,"%f%f%f",&cso->xs,&cso->ys,&cso->zs);
				}
				continue;
			}
			if (strcmp(name,"rotate")==0 && bReadingNode)
			{
				char* b = xml_get_read_buffer(); while (*b!='>') b++; b++; float t,y;
				scene_object *dest;
				if (bReadingPivot)
				{
					dest=pivot;
				}
				else
				{
					dest=cso;
				}
				xml_tag_get_next_attribute(tag,an,av); 
					 if (strcmp(av,"jointOrientX")==0) { sscanf(b,"%f%f%f%f",&y,&t,&t,&dest->jox); if(y<0.0) dest->jox*=-1;}
				else if (strcmp(av,"jointOrientY")==0) { sscanf(b,"%f%f%f%f",&t,&y,&t,&dest->joy); if(y<0.0) dest->joy*=-1;}
				else if (strcmp(av,"jointOrientZ")==0) { sscanf(b,"%f%f%f%f",&t,&t,&y,&dest->joz); if(y<0.0) dest->joz*=-1;}
				else if (strcmp(av,"rotateX")==0) { sscanf(b,"%f%f%f%f",&y,&t,&t,&dest->xr); if(y<0.0) dest->xr*=-1;}
				else if (strcmp(av,"rotateY")==0) { sscanf(b,"%f%f%f%f",&t,&y,&t,&dest->yr); if(y<0.0) dest->yr*=-1;}
				else if (strcmp(av,"rotateZ")==0) { sscanf(b,"%f%f%f%f",&t,&t,&y,&dest->zr); if(y<0.0) dest->zr*=-1;}
				else 
				{
					float vx,vy,vz,rot,avx,avy,avz;
					sscanf(b,"%f%f%f%f",&vx,&vy,&vz,&rot);
					avx = vx; if(avx<0.0) avx=-avx;
					avy = vy; if(avy<0.0) avy=-avy;
					avz = vz; if(avz<0.0) avz=-avz;
					if (avx>avy)
					{
						if (avx>avz)
						{
							if (vx<0.0) rot=-rot;
							dest->xr=rot;
						}
						else
						{
							if (vz<0.0) rot=-rot;
							dest->zr=rot;
						}
					}
					else
					{
						if (avy>avz)
						{
							if (vy<0.0) rot=-rot;
							dest->yr=rot;
						}
						else
						{
							if (vz<0.0) rot=-rot;
							dest->zr=rot;
						}
					}

				}
				continue;
			}
			if (strcmp(name,"instance_geometry")==0 && bReadingNode)
			{
				xml_tag_get_next_attribute(tag,an,av); 
				char* p = av;
				p++;

				for (int i=0; i<cdat->mesh_count; i++)
				{
					mesh* cm = cdat->mesh_list + i;
					char* si = cm->name;
					char* di = p;
					while ( *si == *di && *si!=0 && *di!=0)
					{
						si++;
						di++;
					}
					if (*si == 0 || *di == 0)
					{
						cso->model=cm;
						break;
					}
					else continue;
				}

			}
			
		}
		else
		{
			if (strcmp(name,"visual_scene")==0 && bReadingVisualScene)
			{
				bReadingVisualScene=false;
				continue;
			}
			if (strcmp(name,"library_visual_scenes")==0)
			{
				break;
			}
			if (strcmp(name,"node")==0 && bReadingVisualScene && bReadingNode && !bReadingPivot)
			{
				bReadingNode=false;
				continue;
			}
			if (strcmp(name,"node")==0 && bReadingVisualScene && bReadingNode && bReadingPivot)
			{
				bReadingPivot=false;
				continue;
			}
		}
	}
}

void readanims(collada_data_table* cdat)
{
	char tag[1024],name[1024],an[1024],av[1024];

	scene_object* cso;
	anim* canim;

	bool bReadingAnimGroup=false, bReadingAnim=false;

	while(true)
	{
		xml_get_tag(tag);
		xml_tag_get_name(tag,name);
		if (tag[1] != '/')
		{
			if(strcmp(name,"animation")==0 && !bReadingAnimGroup)
			{
				bReadingAnimGroup=true;
				xml_tag_get_next_attribute(tag,an,av);
				xml_tag_get_next_attribute(tag,an,av);
				assert(cdat->scene_object_count < cdat->scene_object_capacity);
				cso = cdat->scene_object_list + cdat->scene_object_count;
				cdat->scene_object_count++;
				
				char* tn = (char*)malloc(strlen(av)+1);
				strcpy(tn,av);
				cso->name=tn;
				mesh* i = cdat->mesh_list;
				cso->x=cso->y=cso->z=cso->xr=cso->yr=cso->zr=0.0;
				cso->xs=cso->ys=cso->zs=1.0;
				cso->joz=cso->joy=cso->jox=0.0;
				cso->anim_group[0]=cso->anim_group[1]=cso->anim_group[2]=
				cso->anim_group[3]=cso->anim_group[4]=cso->anim_group[5]=0;
				cso->model=0;
				cso->pivot=0;

				for (int j=0; j<cdat->mesh_count; j++)
				{
					char* si = i->name, *di = cso->name;
					while (*si == *di && *si != '-' && *di!=0)
					{
						si++;
						di++;
					}
					if (*si == '-' || *di==0)
					{
						cso->model = i;
						break;
					}
					i++;
				}

				continue;
			}
			if(strcmp(name,"animation")==0 && bReadingAnimGroup)
			{
				bReadingAnim=true;

				assert(cdat->anim_count<cdat->anim_capacity);
				canim = cdat->anim_list + cdat->anim_count;
				cdat->anim_count++;

				continue;
			}
			if (strcmp(name,"sampler")==0 && bReadingAnim)
			{
				xml_tag_get_next_attribute(tag,an,av);
				char* tn = (char*)malloc(strlen(av)+1);
				strcpy(tn,av);
				canim->name=tn;
				continue;
			}
			if (strcmp(name,"source")==0 && bReadingAnim)
			{
				xml_tag_get_next_attribute(tag,an,av);

				char* tok = strtok(av,"-");

				float **fadest=0;
				char axis;
				//bool bTranslate=false, bRotate=false;
				int agoffs=0;

				tok=strtok(0,"-");
				if ( strcmp(tok, "translate")==0)
				{
					tok=strtok(0,"-");
					tok=strtok(0,"-");

					char*p = tok;

					while (*(p+1)!=0)
					{
						p++;
					}

					axis=*p;
					*p=0;

					agoffs+=0;
				}
				else
				{
					char *p = tok;
					while (*(p+1)!=0)
					{
						p++;
					}
					axis=*p;
					*p;

					tok=strtok(0,"-");
					tok=strtok(0,"-");

					p=tok;
					while (!(*p<='Z' && *p>='A'))
					{
						p++;
					}
					*p=0;
					
					agoffs+=3;
				}

				switch(axis)
				{
				case 'X': agoffs+=0; break;
				case 'Y': agoffs+=1; break;
				case 'Z': agoffs+=2; break;
				}

				cso->anim_group[agoffs]=canim;

				if (strcmp(tok,"input")==0)
				{
					fadest = &canim->input;
					goto afadestsrc;
				}
				if (strcmp(tok,"output")==0)
				{
					fadest = &canim->output;
					goto afadestsrc;
				}
				if (strcmp(tok,"intan")==0)
				{
					fadest = &canim->itangent;
					goto afadestsrc;
				}
				if (strcmp(tok,"outtan")==0)
				{
					fadest = &canim->otangent;
					//goto afadestsrc;
				}

afadestsrc:
				if (fadest==0) continue;
				
				while (strcmp(name,"float_array")!=0)
				{
					xml_get_tag(tag);
					xml_tag_get_name(tag,name);
				}

				xml_tag_get_next_attribute(tag,an,av);
				xml_tag_get_next_attribute(tag,an,av);

				int count;

				sscanf(av,"%d",&count);
				canim->count=count;

				*fadest = (float*) malloc(count*sizeof(float));
				float *pf= *fadest;

				FILE *f = xml_get_fileptr();

				for (int i=0; i<count; i++)
				{
					fscanf(f,"%f",pf++);
				}
				
				continue;
			}
		}
		else
		{
			if(strcmp(name,"library_animations")==0)
			{
				break;
			}
			if(strcmp(name,"animation")==0 && bReadingAnimGroup && !bReadingAnim)
			{
				bReadingAnimGroup=false;
				continue;
			}
			if(strcmp(name,"animation")==0 && bReadingAnimGroup && bReadingAnim)
			{
				bReadingAnim=false;
				continue;
			}
		}
	}
}

void load_collada(collada_data_table* cdat, char* filename)
{
	FILE *f = fopen(filename,"r");
	assert(f!=0);
	xml_begin(f);

	bool bReadingGeometryLib=false;
	bool bReadingGeometry=false;
	mesh* cmesh;
	char* dcp;


	char tag[1024],name[1024],an[1024],av[1024];

	while (true)
	{
		xml_get_tag(tag);
		xml_tag_get_name(tag,name);
		if (tag[1] != '/')
		{
			if (strcmp(name,"library_animations")==0)
			{
				readanims(cdat);
				continue;
			}
			if (strcmp(name,"library_visual_scenes")==0)
			{
				readvisualscenes(cdat, filename);
				continue;
			}
			if (strcmp(name,"library_geometries")==0)
			{
				bReadingGeometryLib = true;
				continue;
			}
			if (strcmp(name,"geometry")==0)
			{
				//cmesh = (mesh*) malloc(sizeof(mesh));
				xml_tag_get_next_attribute(tag,an,av);

				bool bFound=false;

				for(int i=0; i<cdat->mesh_count; i++)
				{
					mesh* p = cdat->mesh_list+i;
					char* si = p->name;
					char* di = av;

					while (*si == *di && *si!='-' && *si!=0 && *di!='-' && *di!=0)
					{
						si++;
						di++;
					}
					if (*si=='-' || *si==0 || *di=='-' || *di==0)
					{
						bFound=true;
					}
				}

				if (bFound) continue;

				assert(bReadingGeometryLib);
				assert(cdat->mesh_count +1 < cdat->mesh_capacity);

				bReadingGeometry=true;

				cmesh = cdat->mesh_list + cdat->mesh_count++;
				
				cmesh->glid=0;
				cmesh->normal_list=0;
				cmesh->triangle_list=0;
				cmesh->uv_list=0;
				cmesh->vertex_list=0;
				cmesh->triangle_count=0;
				
				int i = strlen(av)+1;
				dcp=(char*) malloc(i);
				memcpy(dcp,av,i);

				cmesh->name=dcp;
				continue;
			}
			if (strcmp(name,"float_array")==0)
			{
				if (bReadingGeometry&&bReadingGeometryLib)
				{
					xml_tag_get_next_attribute(tag,an,av);
					char* meshid = cmesh->name;
					char* arrayid = av;

					while (true)
					{
						while (*meshid == *arrayid)
						{
							meshid++;
							arrayid++;
						}
						
						if (*meshid != *arrayid)
						{
							if (*meshid == 0) //idok
							{
								float **dest;
								if (arrayid[1] == 'P') //vertex
								{
									dest = &(cmesh->vertex_list);
									
								}
								else
								if (arrayid[1] == 'N') //normal
								{
									dest = &(cmesh->normal_list);
								}
								else
								if (arrayid[1] == 'U') //uv
								{
									dest = &(cmesh->uv_list);
								}
								xml_tag_get_next_attribute(tag,an,av);
								int count;
								sscanf(av,"%d",&count);
								*dest = (float*) malloc(count*sizeof(float));
								float *p = *dest;
								while(count--)
								{
									fscanf(f,"%f",p++);
								}
								break; 
							}
							else //doesn't match
							{
								break;
							}
						}
					}
				}
				continue;
			}
			if (strcmp(name,"polygons")==0)
			{
				if (bReadingGeometry&&bReadingGeometryLib)
				{
					do
					{
						xml_tag_get_next_attribute(tag,an,av);
					} while (strcmp(an,"count")!=0 && *an!=0);
					int count;
					sscanf(av,"%d",&count);

					cmesh->triangle_count=count;
					bool bReadingMeta=true;
					int *tdatadi=0;
					int tc=0;

					//ok let's do this:
					int vo=-1,no=-1,uo=-1;
					int data_per_vertex=0;
					while (true)
					{
						xml_get_tag(tag);
						xml_tag_get_name(tag,name);
						if (strcmp(name,"input")==0)
						{
							data_per_vertex++;
							xml_tag_get_next_attribute(tag,an,av);
							char cc = av[0];
							xml_tag_get_next_attribute(tag,an,av);
							int ofs;
							sscanf(av,"%d",&ofs);
							switch (cc)
							{
								case 'V' :vo=ofs; break;
								case 'N' :no=ofs; break;
								case 'T' :uo=ofs; break;
							}
						}
						else
						if (strcmp(name,"p")==0 && tag[1] !='/')
						{
							if (bReadingMeta)
							{
								cmesh->triangle_list = (int*) malloc(data_per_vertex * 3 * cmesh->triangle_count * sizeof(int));
								tdatadi=cmesh->triangle_list;
								bReadingMeta=false;
							}
							//dear god...
							char* si = xml_get_read_buffer();
							while (*si != '>') si++;
							si++;
							int ibuffer[16];
							int *ip;

							tc++;

							for (int j=0; j<3; j++)
							{
								ip=ibuffer;
								for (int i=0; i<data_per_vertex; i++)
								{
									sscanf(si,"%d",ip++);
									while (*si != ' ' && *si != '<') si++;
									while (*si == ' ') si++;
								}
								if (vo!=-1) 
									*(tdatadi++)=*(ibuffer+vo); 
								else 
									*(tdatadi++)=-1;
								if (no!=-1) 
									*(tdatadi++)=*(ibuffer+no); 
								else 
									*(tdatadi++)=-1;
								if (uo!=-1) 
									*(tdatadi++)=*(ibuffer+uo); 
								else 
									*(tdatadi++)=-1;
							}

							if (tc == cmesh->triangle_count) 
								break;
							
						}
					}

				}
				continue;
			}
		}
		else
		{
			if (strcmp(name,"COLLADA")==0)
			{
				break;
			}
			if (strcmp(name,"library_geometries")==0)
			{
				bReadingGeometryLib = false;
				continue;
			}
			if (strcmp(name,"geometry")==0)
			{
				bReadingGeometry=false;
				cmesh=0;
				continue;
			}

		}

	}

	fclose(f);
}

