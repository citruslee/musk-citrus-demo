#include <math.h>

float linear(const float t, const float p0, const float p1)
{
	return p0+t*(p1-p0);
}

float smoothstep(const float t, const float p0, const float p1)
{
	float x = t*t*(3.0f - 2.0f*x);
	return p0+x*(p1-p0);
}

float smootherstep(const float t, const float p0, const float p1)
{
	float x = t*t*t*(t*(t*6.0f-15.0f)+10.0f);
	return p0+x*(p1-p0);
}

float cubic_spline(const float t, const float p0, const float p1, const float p2, const float p3)
{
	float t2=t*t;
	float a= p3-p2-p0+p1;
	float b= p0-p1-a;
	float c= p2-p0;
	float d= p1;
	return t2*t*a+t2*b+t*c+d;
}

float catmull_rom(const float t, const float p0, const float p1, const float p2, const float p3)
{
	return 
		0.5f*(
			(2.0f*p1)+
			(-p0+p2)*t+
			(2.0f*p0 - 5.0f*p1 + 4.0f*p2 - p3) *t*t +
			(-p0 + 3.0f*p1 -3.0f*p2 +p3) *t*t*t
			);
}

float bezier(const float t, const float p0, const float c0, const float c1, const float p1)
{
	float c = 3.0f*(c0-p0);
	float b = 3.0f*(c1-c0)-c;
	float a = p1-p0-c-b;
	float t2= t*t;
	
	return a*t2*t + b*t2 + c*t + p0;
}

float hermite(const float t, const float p0, const float p1, const float ta0, const float ta1)
{
	float t2 = t*t;
	float t3 = t2*t;
	return
		p0*(2.0f*t3 - 3.0f*t2 + 1.0f)+
		ta0*(t3 - 2.0f*t2 + t)+
		p1*(-2.0f*t3 + 3.0f*t2)+
		ta1*(t3-t2);
}



/*

q(t) = 0.5 *(   	(2 * P1) +
  	(-P0 + P2) * t +
(2*P0 - 5*P1 + 4*P2 - P3) * t2 +
(-P0 + 3*P1- 3*P2 + P3) * t3)

*/

float sample_linear(const float t, const float* data,const int nr)
{
	int p0,p1;
	if (nr==0)
	{
		return t;
	}
	else if (nr==1) 
	{
		return *data;
	}
	else if (t<0)
	{
		p0=0; p1=1;
	}
	else if (t>nr-1)
	{
		p0=nr-2;
		p1=nr-1;
	}
	else
	{
		p0=floor(t);
		p1=p0+1;
	}
	float u=t-p0;
	return data[p1]*u+data[p0]*(1-u);
}

float sample_cubic(const float t, const float* data, const int nr)
{
	float p0,p1,p2,p3;
	if (nr<=2 || t<0.0 || t>nr-1)
	{
		return sample_linear(t,data,nr);
	}
	else if(t<1.0)
	{
		p1=data[0]; 
		p2=data[1]; 
		p3=data[2];
		p0=p1+p1-p2;
	}
	else if(t>nr-2)
	{
		p0=data[nr-3];
		p1=data[nr-2];
		p2=data[nr-1];
		p3=p2+p2-p1;
	}
	else
	{
		int s=floor(t);
		p0=data[s-1];
		p1=data[s];
		p2=data[s+1];
		p3=data[s+2];
	}
	return cubic_spline(fmod(t,1.0f),p0,p1,p2,p3);
}

float sample_catmull(const float t, const float* data, const int nr)
{
	float p0,p1,p2,p3;
	if (nr<=2 || t<0.0 || t>nr-1)
	{
		return sample_linear(t,data,nr);
	}
	else if(t<1.0)
	{
		p1=data[0]; 
		p2=data[1]; 
		p3=data[2];
		p0=p1+p1-p2;
	}
	else if(t>nr-2)
	{
		p0=data[nr-3];
		p1=data[nr-2];
		p2=data[nr-1];
		p3=p2+p2-p1;
	}
	else
	{
		int s=floor(t);
		p0=data[s-1];
		p1=data[s];
		p2=data[s+1];
		p3=data[s+2];
	}
	return catmull_rom(fmod(t,1.0f),p0,p1,p2,p3);
}

float sample_bezier(const float t, const float* pos, const int nr, const float* cp_in, const float* cp_out)
{
	if (nr <= 0)
	{
		return t;
	}
	else if (t<0.0f)
	{
		return linear(t+1.0f,*pos+*cp_in,*pos);
	}
	else if (t>nr-1)
	{
		return linear(t-nr+1.0f,*(pos+nr-1),*(pos+nr-1)+*(cp_out+nr-1));
	}
	else
	{
	
		float ft=floor(t);
		int i=(int)ft;

		return bezier(t-ft,*(pos+i),*(cp_out+i),*(cp_in+i+1),*(pos+i+1));
	}
}

float sample_bezier_dcp(const float t, const float* pos, const int nr, const float* cp_in, const float* cp_out)
{
	if (nr <= 0)
	{
		return t;
	}
	else if (t<0.0f)
	{
		return linear(t+1.0f,*pos+*cp_in,*pos);
	}
	else if (t>nr-1)
	{
		return linear(t-nr+1.0f,*(pos+nr-1),*(pos+nr-1)+*(cp_out+nr-1));
	}
	else
	{
	
		float ft=floor(t);
		int i=(int)ft;

		return bezier(t-ft,*(pos+i),*(pos+i)+*(cp_out+i),*(pos+i+1)-*(cp_in+i+1),*(pos+i+1));
	}
}

float sample_hermite(const float t, const float* pos, const int nr, const float* tan_in, const float* tan_out)
{
	if (nr <= 0)
	{
		return t;
	}
	else if (t<0.0f)
	{
		return linear(t+1.0f,*pos+*tan_in,*pos);
	}
	else if (t>nr-1)
	{
		return linear(t-nr+1.0f,*(pos+nr-1),*(pos+nr-1)+*(tan_out+nr-1));
	}
	else
	{
		float ft=floor(t);
		int i=(int)ft;

		return hermite(t-ft,*(pos+i),*(pos+i+1),*(tan_out+i),*(tan_in+i+1));
	}
}

float sample_inverse_linear(const float t, const float* data, const int nr)
{
	//assumes data[n+1]>a[n]
	int seg=0;
	while (t>data[seg+1]&&seg+1<nr-1) {seg++;}
	
	float p = t-data[seg];
	p/=data[seg+1]-data[seg];
	return seg+p;
}


float sample_custom(const float t, const float* pos, const int nr)
{
	if (nr <= 0)
	{
		return t;
	}
	else if (t<0.0f)
	{
		return *(pos);
	}
	else if (t>nr-1)
	{
		return *(pos+nr-1);
	}
	else
	{
		float ft=floor(t);
		int i=(int)ft;

		return smootherstep(t-ft,*(pos+i),*(pos+i+1));
	}
}


float sample_inverse_catmull(const float t, const float* data, const int nr)
{
	//assumes data[n+1]>a[n]
	int seg=0;
	while (t>data[seg+1]&&seg+1<nr-1) {seg++;}
	
	float p = t-data[seg];
	float l=data[seg+1]-data[seg];
	p=p/l;
	p+=seg;

	float r,e,d;
	e=1.0;	

	r=sample_catmull(p,data,nr);
	d=abs(r-t)/l;
	if (r<t) p+=d*e;
	else p-=d*e;
	e*=0.5;

	r=sample_catmull(p,data,nr);
	d=abs(r-t)/l;
	if (r<t) p+=d*e;
	else p-=d*e;
	e*=0.5;

	r=sample_catmull(p,data,nr);
	d=abs(r-t)/l;
	if (r<t) p+=d*e;
	else p-=d*e;
	e*=0.5;

	r=sample_catmull(p,data,nr);
	d=abs(r-t)/l;
	if (r<t) p+=d*e;
	else p-=d*e;
	e*=0.5;

	return p;
}


float sample_inverse_custom(const float t, const float* data, const int nr)
{
	//assumes data[n+1]>a[n]
	int seg=0;
	while (t>data[seg+1]&&seg+1<nr-1) {seg++;}
	
	float p = t-data[seg];
	float l=data[seg+1]-data[seg];
	p=p/l;
	p+=seg;

	float r,e,d;
	e=1.0;	

	r=sample_custom(p,data,nr);
	d=abs(r-t)/l;
	if (r<t) p+=d*e;
	else p-=d*e;
	e*=0.5;

	r=sample_custom(p,data,nr);
	d=abs(r-t)/l;
	if (r<t) p+=d*e;
	else p-=d*e;
	e*=0.5;

	r=sample_custom(p,data,nr);
	d=abs(r-t)/l;
	if (r<t) p+=d*e;
	else p-=d*e;
	e*=0.5;

	r=sample_custom(p,data,nr);
	d=abs(r-t)/l;
	if (r<t) p+=d*e;
	else p-=d*e;
	e*=0.5;

	return p;
}
