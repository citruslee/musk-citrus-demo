#undef UNICODE
#include <Windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include "myext.h"
#include "Resource.h"
#include "ResourceTypes.h"
#include "globjects.h"
#include "Curves.h"
#include "xmath.h"

#pragma comment (lib,"Opengl32.lib")
#pragma comment (lib,"GLU32.lib")

void InitResources()
{
	InitGlExtensions();
}
