#undef UNICODE
#include <Windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include "myext.h"
#include <time.h>
#include <math.h>

#include <stdio.h>

#pragma comment (lib,"Opengl32.lib")

class Shader
{
public:
	unsigned id;
	char* source;
	char* source_file;
	int source_len;
	Shader(unsigned type)
	{
		source = 0;
		source_len =0;
		id = glCreateShader(type);
	}

	Shader(unsigned type, char* filename)
	{
		source = 0;
		source_len =0;
		id = glCreateShader(type);
		LoadSource(filename);
	}

	~Shader()
	{
		if (source!=0)
		{
			delete source;
			source = 0;
			source_len =0;
		}
	}

	void LoadSource(char* filename)
	{
		FILE *f = fopen(filename,"r");
		if (f==0)
		{
			MessageBox(0,filename,"File Read Error",MB_OK);
			return;
		}
		fseek(f,0,SEEK_END);
		int size = ftell(f);

		fseek(f,0,SEEK_SET);
		source = new char[size+1];
		source_len=size;
		int endp = fread(source,1,size,f);																																						
		source[endp]=0;
		fclose(f);
		glShaderSource(id,1,&source,&source_len);
		glCompileShader(id);
		char log[1024];
		glGetInfoLog(id,1024,0,log); if (log[0]) MessageBox(0,log,"SHADER error",MB_OK);
	}
};

class VertexShader : public Shader
{
public:
	VertexShader(char* filename) : Shader(GL_VERTEX_SHADER,filename)
	{
		
	}
};

class FragmentShader : public Shader
{
public:
	FragmentShader(char* filename) : Shader(GL_FRAGMENT_SHADER,filename)
	{
		
	}
};

class GpuProgram
{
public:
	unsigned id;

	GpuProgram()
	{
		id = glCreateProgram();
	}

	GpuProgram(Shader* s0, Shader* s1 = 0, Shader* s2 = 0, Shader* s3 = 0, Shader* s4 = 0, Shader* s5 = 0, Shader* s6 = 0, Shader* s7 = 0)
	{
		id = glCreateProgram();
		if (s0!=0) AttachShader(s0);
		if (s1!=0) AttachShader(s1);
		if (s2!=0) AttachShader(s2);
		if (s3!=0) AttachShader(s3);
		if (s4!=0) AttachShader(s4);
		if (s5!=0) AttachShader(s5);
		if (s6!=0) AttachShader(s6);
		if (s7!=0) AttachShader(s7);
		this->Link();
	}

	void AttachShader(Shader *target)
	{
		glAttachShader(id,target->id);
	}

	void Link()
	{
		glLinkProgram(id);
		char log[1024];
		glGetInfoLog(id,1024,0,log); if (log[0]) MessageBox(0,log,"Shader link error",MB_OK);
	}

};

void glBindProgram(GpuProgram *prg)
{
	if (prg != 0)
	{
		glUseProgram(prg->id);
	}
	else
	{
		glUseProgram(0);
	}
}

class FBO
{
public:
	unsigned hfb,hrbc,htex;

	FBO()
	{
		hfb=0;
		hrbc=0;
		htex=0;
	}
	FBO(unsigned width, unsigned height,bool bmipmap = 1, bool bdepth = 0)
	{
		glGenFramebuffersEXT(1, &hfb);
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, hfb);

		glGenTextures(1,&htex);
		glBindTexture(GL_TEXTURE_2D,htex);
		if (bmipmap)
		{	
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);
		glFramebufferTexture2DEXT( GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, htex, 0 );
		glGenerateMipmapEXT(GL_TEXTURE_2D);
	}
	void GenerateMipmap()
	{
		glBindTexture(GL_TEXTURE_2D,this->htex);
		glGenerateMipmapEXT(GL_TEXTURE_2D);
	}

};

void glBindFramebuffer(FBO* x)
{
	if (x != 0)
	{	
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,x->hfb);
	}
	else
	{
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,0);	
	}
}

void glBindTexture(FBO* x)
{
	if (x == 0)
	{
		glBindTexture(GL_TEXTURE_2D,0);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D,x->htex);
	}
}

void glBindTexture(FBO* x,unsigned texture_unit)
{
	glActiveTexture(texture_unit);
	glBindTexture(x);
}

void qq()
{
	glBegin(GL_TRIANGLES);
	glColor3f(0.1,0.1,0.1);
	glVertex2f(0.0,1.3);
	glColor3f(0,0.1,0.25);
	glVertex2f(-1.0,-1.0);
	glColor3f(0,0.25,0.15);
	glVertex2f(+1.0,-1.0);
	glEnd();
}

void render()
{
	glEnable(GL_MULTISAMPLE_ARB);
	glClearColor(0.1,0.2,0.3,1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glScalef(0.1,0.1,0.1);
	for (int y=-10;y<=10;y++)
	for (int x=-10;x<=10;x++)
	{
		glPushMatrix();
		glTranslatef(x,-y,0);
		glRotatef(x*12+y*135+clock()*0.07+(1+sin(clock()*0.003))*20,0,0,1);
		glScalef(1.2,1.2,1.2);
		qq();
		glPopMatrix();
	}

	glDisable(GL_MULTISAMPLE_ARB);

		glEnable(GL_MULTISAMPLE_ARB);
	//glClearColor(0,0,0,1);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	
	glRotatef(clock()*0.01,0,0,1);
	glScalef(0.7,0.7,1.0);
	glBegin(GL_TRIANGLES);
	glColor3f(1,0,0);
	glVertex2f(0,-1);
	glColor3f(0,1,0);
	glVertex2f(+1,+1);
	glColor3f(0,0,1);
	glVertex2f(-1,+1);
	glEnd();

	glDisable(GL_MULTISAMPLE_ARB);

}

void sheet()
{
	glBegin(GL_QUADS);
	glTexCoord2f(0,0); glVertex2f(0,0);
	glTexCoord2f(1,0); glVertex2f(1,0);
	glTexCoord2f(1,1); glVertex2f(1,1);
	glTexCoord2f(0,1); glVertex2f(0,1);
	glEnd();
}

void sheetouv(float u,float v)
{
	glBegin(GL_QUADS);
	glTexCoord2f(0+u,0+v); glVertex2f(0,0);
	glTexCoord2f(1+u,0+v); glVertex2f(1,0);
	glTexCoord2f(1+u,1+v); glVertex2f(1,1);
	glTexCoord2f(0+u,1+v); glVertex2f(0,1);
	glEnd();
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
    {
        case WM_CLOSE:
            DestroyWindow(hwnd);
        break;
        case WM_DESTROY:
            PostQuitMessage(0);
        break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

int main()
{
	int r;
	WNDCLASS wc;
	HWND hwnd;
	char* cn = "hehehe";
	memset(&wc,0,sizeof(WNDCLASS));
	wc.lpszClassName=cn;
	wc.lpfnWndProc=DefWindowProc;

	int res = RegisterClass(&wc);
	hwnd = CreateWindow(cn,"Wind",0,0,0,800,600,0,0,0,0);
	ShowWindow(hwnd,1);

	static  PIXELFORMATDESCRIPTOR pfd=                  // pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),                  // Size Of This Pixel Format Descriptor
		1,                              // Version Number
		PFD_DRAW_TO_WINDOW |                        // Format Must Support Window
		PFD_SUPPORT_OPENGL |                        // Format Must Support OpenGL
		PFD_DOUBLEBUFFER,                       // Must Support Double Buffering
		PFD_TYPE_RGBA,                          // Request An RGBA Format
		24,                               // Select Our Color Depth
		0, 0, 0, 0, 0, 0,                       // Color Bits Ignored
		0,                              // No Alpha Buffer
		0,                              // Shift Bit Ignored
		0,                              // No Accumulation Buffer
		0, 0, 0, 0,                         // Accumulation Bits Ignored
		16,                             // 16Bit Z-Buffer (Depth Buffer)
		0,                              // No Stencil Buffer
		0,                              // No Auxiliary Buffer
		PFD_MAIN_PLANE,                         // Main Drawing Layer
		0,                              // Reserved
		0, 0, 0                             // Layer Masks Ignored
	};

	HDC dc = GetDC(hwnd);

	int iPixelFormat = ChoosePixelFormat(dc, &pfd); 
	r = SetPixelFormat(dc,iPixelFormat,&pfd);

	HGLRC rc =  wglCreateContext(dc);
	wglMakeCurrent(dc,rc);

	//got context, get extensions
	if (!InitGlExtensions())
	{
		return 1;
	}

	int valid;
	int pf;
	unsigned numFormats;
	float fAttributes[] = {0,0};
	 int iAttributes[] = { WGL_DRAW_TO_WINDOW_ARB,GL_TRUE,
        WGL_SUPPORT_OPENGL_ARB,GL_TRUE,
        WGL_ACCELERATION_ARB,WGL_FULL_ACCELERATION_ARB,
        WGL_COLOR_BITS_ARB,24,
        WGL_ALPHA_BITS_ARB,8,
        WGL_DEPTH_BITS_ARB,16,
        WGL_STENCIL_BITS_ARB,0,
        WGL_DOUBLE_BUFFER_ARB,GL_TRUE,
        WGL_SAMPLE_BUFFERS_ARB,GL_TRUE,
        WGL_SAMPLES_ARB, 4 ,                        // Check For 4x Multisampling
        0,0};
 
    // First We Check To See If We Can Get A Pixel Format For 4 Samples
	 iAttributes[19] = 8; //8 samples
	valid = wglChoosePixelFormatARB(dc,iAttributes,fAttributes,1,&pf,&numFormats);
	if (!valid)
	{
		iAttributes[19] = 4; //4 samples
		valid = wglChoosePixelFormatARB(dc,iAttributes,fAttributes,1,&pf,&numFormats);
	}
	if (!valid)
	{
		iAttributes[19] = 2; //2 samples
		valid = wglChoosePixelFormatARB(dc,iAttributes,fAttributes,1,&pf,&numFormats);
	}
	if (!valid)
	{
		goto endofsetup;
	}


	wglDeleteContext(rc);
	
	DestroyWindow(hwnd);
	hwnd = CreateWindow(cn,"Wind",0,0,0,800,600,0,0,0,0);	
	ShowWindow(hwnd,1);
	dc=GetDC(hwnd);

//	iPixelFormat = ChoosePixelFormat(dc, &pfd); 	
	r = SetPixelFormat(dc,pf,&pfd);

	rc = wglCreateContext(dc);
	wglMakeCurrent(dc,rc);

endofsetup:
	
	
	/////// fbo
	/*
	unsigned tex,ColorBufferID;
	unsigned fb;

	//framebuffah
	glGenFramebuffersEXT(1, &fb);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);

	//color t
	glGenTextures(1,&tex);
	glBindTexture(GL_TEXTURE_2D,tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmapEXT(GL_TEXTURE_2D);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 800, 600, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);
	glFramebufferTexture2DEXT( GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex, 0 );
	//NULL means reserve texture memory, but texels are undefined*/

	FBO buffer(800,600);
	FBO buffer2(800,600);
	FBO buffer3(800,600);

	//multisample buffer
	
	
	
	//-------------------------
	//glGenRenderbuffersEXT(1, &depth_rb);
	//glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depth_rb);
	//glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, 256, 256);
	//-------------------------
	//Attach depth buffer to FBO
	//glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depth_rb);
	//-------------------------
	//Does the GPU support current FBO configuration?

	int status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
	{
		int derp = 5;
	}



	printf("%s\n",glGetString(GL_VENDOR));
	printf("%s\n",glGetString(GL_RENDERER));
	printf("%s\n",glGetString(GL_VERSION));
	printf("%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
	printf("%s\n",glGetString(GL_EXTENSIONS));


	GpuProgram *nprg = new GpuProgram(new FragmentShader("test.frag"));
	int uamploc = glGetUniformLocation(nprg->id,"amplitude");
	int ufreloc = glGetUniformLocation(nprg->id,"frequency");

	GpuProgram *blurx = new GpuProgram(new FragmentShader("blur_x.frag"));
	GpuProgram *blury = new GpuProgram(new FragmentShader("blur_y.frag"));
	GpuProgram *postprg = new GpuProgram(new FragmentShader("postprg.frag"));

	float lt = 0;

	while (!GetAsyncKeyState(VK_ESCAPE))
	{
		//fb
		//glBindFramebuffer(&buffer);
		//glViewport( 0, 0, 800, 600 );

	//	render();
		float t = clock()*0.001;
		float dt = t-lt;
		lt = t;
		
		static float var_param_0=0,var_param_1=0;
		if (GetAsyncKeyState('Q')){var_param_0+=dt;}
		if (GetAsyncKeyState('A')){var_param_0-=dt;}
		if (GetAsyncKeyState('W')){var_param_1+=dt;}
		if (GetAsyncKeyState('S')){var_param_1-=dt;}

		//screen
		glBindProgram(0);
		
		glBindFramebuffer(0);
		glViewport( 0, 0, 800, 600 );

		glClearColor(0.1,0.4,0.3,1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		render();

		//done, let the post begin

		glEnable(GL_TEXTURE_2D);

		glBindTexture(&buffer);
		//int err = glGetError();


		glCopyTexImage2D(GL_TEXTURE_2D,0, GL_RGB8, 0,0,800,600,0);
	//	err = glGetError();
		
		buffer.GenerateMipmap();

		//glGenerateMipmapEXT(GL_TEXTURE_2D);
		
		//glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_LOD_BIAS,5.0);


		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glTranslatef(-1,-1,0);
		glScalef(2,2,2);

		glEnable(GL_BLEND);

		glBindTexture(&buffer);
		glBindFramebuffer(&buffer3);
		
		glBlendFunc(GL_ONE,GL_ZERO);

		glColor4f(1.00,1.00,1.00,1.0);
		
		glBindProgram(nprg);
		glUniform1f(uamploc,pow(2.0f,var_param_0));
		glUniform1f(ufreloc,pow(2.0f,var_param_1));

		//sheetouv(0,0);

		glBindTexture(&buffer3);
		glBindFramebuffer(&buffer);

		//glBindProgram(0);
		glBindProgram(blurx);
		sheetouv(0,0);

		glBindTexture(&buffer);
		glBindFramebuffer(&buffer2);

		//glBindProgram(0);
		glBindProgram(blury);
		sheetouv(0,0);

		glBindTexture(&buffer3);
		glBindFramebuffer(&buffer2);

		//glBindProgram(0);
		glBindProgram(0);
		glBlendFunc(GL_ONE,GL_ONE);
		sheetouv(0,0);

		glBindTexture(&buffer2);
		glBindFramebuffer(0);

		glDisable(GL_BLEND);
		glBlendFunc(GL_ZERO,GL_ZERO);
		//glBindProgram(0);
		glBindProgram(postprg);
		sheetouv(0,0);
	

		/*
		glColor4f(0.525,0.525,0.525,1.0);
		sheetouv(1.0/800*5*5,0);
		sheetouv(0,1.0/600*5*5);
		sheetouv(-1.0/800*5*5.1,0);
		sheetouv(0,-1.0/600*5*5);

		glColor4f(0.25,0.25,0.25,1.0);
		sheetouv(2.0/800*5*5,0);
		sheetouv(0,2.0/600*5*5);
		sheetouv(-2.0/800*5*5.1,0);
		sheetouv(0,-2.0/600*5*5);
		*/
	/*	glBegin(GL_QUADS);
		glColor4f(2.0,1.0,1.0,1.0);
		glTexCoord2f(0,0); glVertex2f(0,0);
		glTexCoord2f(1,0); glVertex2f(1,0);
		glColor4f(2.0,1.0,1.0,0.0);
		glTexCoord2f(1,1); glVertex2f(1,1);
		glTexCoord2f(0,1); glVertex2f(0,1);
		glEnd();
		*/
		glColor4f(1.0,1.0,1.0,1.0);
		
		glDisable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		
		SwapBuffers(dc);

		MSG msg;
		while(PeekMessage(&msg, NULL, 0, 0, 1))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	r = glGetError();
	return 0;
}