#undef UNICODE
#include <Windows.h>
#include <gl\GL.h>
#include <stdio.h>

#pragma comment(lib,"Opengl32.lib");

int main()
{
	int r;
	WNDCLASS wc;
	HWND hwnd;
	char* cn = "hehehe";
	memset(&wc,0,sizeof(WNDCLASS));
	wc.lpszClassName=cn;
	wc.lpfnWndProc=DefWindowProc;

	int res = RegisterClass(&wc);
	hwnd = CreateWindow(cn,"Wind",0,0,0,800,600,0,0,0,0);
	ShowWindow(hwnd,1);

	static  PIXELFORMATDESCRIPTOR pfd=                  // pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),                  // Size Of This Pixel Format Descriptor
		1,                              // Version Number
		PFD_DRAW_TO_WINDOW |                        // Format Must Support Window
		PFD_SUPPORT_OPENGL |                        // Format Must Support OpenGL
		PFD_DOUBLEBUFFER,                       // Must Support Double Buffering
		PFD_TYPE_RGBA,                          // Request An RGBA Format
		24,                               // Select Our Color Depth
		0, 0, 0, 0, 0, 0,                       // Color Bits Ignored
		0,                              // No Alpha Buffer
		0,                              // Shift Bit Ignored
		0,                              // No Accumulation Buffer
		0, 0, 0, 0,                         // Accumulation Bits Ignored
		16,                             // 16Bit Z-Buffer (Depth Buffer)
		0,                              // No Stencil Buffer
		0,                              // No Auxiliary Buffer
		PFD_MAIN_PLANE,                         // Main Drawing Layer
		0,                              // Reserved
		0, 0, 0                             // Layer Masks Ignored
	};

	HDC dc = GetDC(hwnd);

	int iPixelFormat = ChoosePixelFormat(dc, &pfd); 
	r = SetPixelFormat(dc,iPixelFormat,&pfd);

	HGLRC rc =  wglCreateContext(dc);
	wglMakeCurrent(dc,rc);

	


	printf("%s\n",glGetString(GL_VENDOR));
	printf("%s\n",glGetString(GL_RENDERER));
	printf("%s\n",glGetString(GL_VERSION));
	printf("%s\n",glGetString(0x8B8C));

	char* c = (char*) glGetString(GL_EXTENSIONS);

	while (*c != 0)
	{
		switch (*c)
		{
			case ' ': printf("\n");
			default : printf("%c",*c);
		}
		c++;
	}

	void* x = wglGetProcAddress("glCompileShaderARB");
	void* y = wglGetProcAddress("glCreateProgramARB");
	unsigned u = GetLastError();
	char buffer[1024];
	FormatMessage(0,0,u,0,buffer,1024,0);

}