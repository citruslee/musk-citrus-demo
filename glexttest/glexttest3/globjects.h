
#include <stdio.h>
#include "myext.h"

namespace gl
{
	const int MAX_TEXTURE_UNITS

	class Shader
	{
	public:
		unsigned id;
		char* source;
		char* source_file;
		int source_len;
		Shader(unsigned type)
		{
			source = 0;
			source_len =0;
			id = glCreateShader(type);
		}

		Shader(unsigned type, char* filename)
		{
			source = 0;
			source_len =0;
			id = glCreateShader(type);
			LoadSource(filename);
		}

		~Shader()
		{
			if (source!=0)
			{
				delete source;
				source = 0;
				source_len =0;
			}
		}

		void LoadSource(char* filename)
		{
			FILE *f = fopen(filename,"r");
			if (f==0)
			{
				MessageBox(0,filename,"File Read Error",MB_OK);
				return;
			}
			fseek(f,0,SEEK_END);
			int size = ftell(f);

			fseek(f,0,SEEK_SET);
			source = new char[size+1];
			source_len=size;
			int endp = fread(source,1,size,f);																																						
			source[endp]=0;
			fclose(f);
			glShaderSource(id,1,&source,&source_len);
			glCompileShader(id);
			char log[1024];
			glGetInfoLog(id,1024,0,log); if (log[0]) MessageBox(0,log,"SHADER error",MB_OK);
		}
	};

	class VertexShader : public Shader
	{
	public:
		VertexShader(char* filename) : Shader(GL_VERTEX_SHADER,filename)
		{
		
		}
	};

	class FragmentShader : public Shader
	{
	public:
		FragmentShader(char* filename) : Shader(GL_FRAGMENT_SHADER,filename)
		{
		
		}
	};

	class Program
	{
	public:
		unsigned id;

		GpuProgram()
		{
			id = glCreateProgram();
		}

		GpuProgram(Shader* s0, Shader* s1 = 0, Shader* s2 = 0, Shader* s3 = 0, Shader* s4 = 0, Shader* s5 = 0, Shader* s6 = 0, Shader* s7 = 0)
		{
			id = glCreateProgram();
			if (s0!=0) AttachShader(s0);
			if (s1!=0) AttachShader(s1);
			if (s2!=0) AttachShader(s2);
			if (s3!=0) AttachShader(s3);
			if (s4!=0) AttachShader(s4);
			if (s5!=0) AttachShader(s5);
			if (s6!=0) AttachShader(s6);
			if (s7!=0) AttachShader(s7);
			this->Link();
		}

		void AttachShader(Shader *target)
		{
			glAttachShader(id,target->id);
		}

		void Link()
		{
			glLinkProgram(id);
			char log[1024];
			glGetInfoLog(id,1024,0,log); if (log[0]) MessageBox(0,log,"Shader link error",MB_OK);
		}

	};

	class Texture
	{
	public:
		unsigned id;
	}

	class Uniform
	{
		unsigned location;
	};

	class TextureUnitClass
	{
		unsigned index;
		unsigned texture,sampler,uniform;

		reset()
		{
			texture = 0;
			sampler = 0;
			uniform = 0;
		}

	};

	class TextureUnitContainerClass
	{
		const int size = MAX_TEXTURE_UNITS;
		TextureUnitClass tu[size];
		
	}

	extern TextureUnitContainerClass TextureUnit;

}