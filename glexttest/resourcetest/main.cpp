#undef UNICODE


#include <Windows.h>
#include "Demo.h"
#include <time.h>
#pragma comment (lib,"Opengl32.lib")


int __stdcall WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, char* cmd, int show)
{
	int r;
	WNDCLASS wc;
	HWND hwnd;
	char* cn = "hehehe";
	memset(&wc,0,sizeof(WNDCLASS));
	wc.lpszClassName=cn;
	wc.lpfnWndProc=DefWindowProc;

	int res = RegisterClass(&wc);
	hwnd = CreateWindow(cn,"Wind",0,0,0,800,600,0,0,0,0);
	ShowWindow(hwnd,1);

	static  PIXELFORMATDESCRIPTOR pfd=                  // pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),                  // Size Of This Pixel Format Descriptor
		1,                              // Version Number
		PFD_DRAW_TO_WINDOW |                        // Format Must Support Window
		PFD_SUPPORT_OPENGL |                        // Format Must Support OpenGL
		PFD_DOUBLEBUFFER,                       // Must Support Double Buffering
		PFD_TYPE_RGBA,                          // Request An RGBA Format
		24,                               // Select Our Color Depth
		0, 0, 0, 0, 0, 0,                       // Color Bits Ignored
		0,                              // No Alpha Buffer
		0,                              // Shift Bit Ignored
		0,                              // No Accumulation Buffer
		0, 0, 0, 0,                         // Accumulation Bits Ignored
		16,                             // 16Bit Z-Buffer (Depth Buffer)
		0,                              // No Stencil Buffer
		0,                              // No Auxiliary Buffer
		PFD_MAIN_PLANE,                         // Main Drawing Layer
		0,                              // Reserved
		0, 0, 0                             // Layer Masks Ignored
	};

	HDC dc = GetDC(hwnd);

	int iPixelFormat = ChoosePixelFormat(dc, &pfd); 
	r = SetPixelFormat(dc,iPixelFormat,&pfd);

	HGLRC rc =  wglCreateContext(dc);
	wglMakeCurrent(dc,rc);

	//got context, get extensions
	/*if (!InitGlExtensions())
	{
		return 1;
	}*/

	initDemo();

	resolution[0] = 800;
	resolution[1] = 600;

	while (!GetAsyncKeyState(VK_ESCAPE)  )
	{
		t = clock()*0.001;
		runDemo();

		SwapBuffers(dc);

		MSG msg;
		while(PeekMessage(&msg, NULL, 0, 0, 1))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return 0;
}