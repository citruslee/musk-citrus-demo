float t;
float resolution[2];

#include "../resources/resources.h"


gl::Program *sh0;

void initDemo()
{
	InitGlExtensions();

	sh0 = new gl::Program(new gl::FragmentShader("sh0.frag"));
}

void runDemo()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	sh0->Use();
	sh0->Uniform("iGlobalTime",t);
	sh0->Uniform("iResolution[0]",resolution[0]);
	sh0->Uniform("iResolution[1]",resolution[1]);

	glBegin(GL_QUADS);
	glVertex2f(-1.0f,-1.0f);
	glVertex2f(+1.0f,-1.0f);
	glVertex2f(+1.0f,+1.0f);
	glVertex2f(-1.0f,+1.0f);
	glEnd();

}